package com.example.harkkatyo;

// This class contains methods to save meals and reviews in XML file.
// Method returns only string so have to use different method to write that string in a file.

import android.content.Context;
import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;

public class XMLhelper {

    private XMLhelper() {
    }

    // Source: https://stackoverflow.com/questions/9869507/how-to-create-an-xml-using-xmlserializer-in-android-app
    // https://thedeveloperworldisyours.com/android/write-xml-in-android/

    public static String writeXML(Context context) {

        XmlSerializer xmlS = Xml.newSerializer();
        StringWriter writer = new StringWriter();


        try {
            xmlS.setOutput(writer);

            // Start document.
            xmlS.startDocument("UTF-8", true);
            xmlS.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            // Open tag.
            xmlS.startTag("", "file");

            ArrayList<Meal> mealList = DatabaseSearcher.searchMeals(DatabaseInsert.getDatabase());

            for (Meal m : mealList) {

                // Start writing new meal.
                xmlS.startTag("", "Meal");

                // Add content.
                xmlS.startTag("", "mealID");
                xmlS.text(m.getId());
                xmlS.endTag("", "mealID");

                xmlS.startTag("", "restaurantID");
                xmlS.text(m.getRestaurantID());
                xmlS.endTag("", "restaurantID");

                xmlS.startTag("", "Name");
                xmlS.text(m.getName());
                xmlS.endTag("", "Name");

                xmlS.startTag("", "Type");
                xmlS.text(m.getTypeSring());
                xmlS.endTag("", "Type");

                // Write meals reviews.
                m.updateReviewList();
                for (Review r : m.getReviewList()) {

                    // Start writing new review.
                    xmlS.startTag("", "Review");

                    // Add content.
                    xmlS.startTag("", "reviewID");
                    xmlS.text(r.getReviewID());
                    xmlS.endTag("", "reviewID");

                    xmlS.startTag("", "mealID");
                    xmlS.text(r.getMeal());
                    xmlS.endTag("", "mealID");

                    xmlS.startTag("", "Grade");
                    xmlS.text(r.getGrade());
                    xmlS.endTag("", "Grade");

                    xmlS.startTag("", "VerbalEvaluation");
                    xmlS.text(r.getVerbal());
                    xmlS.endTag("", "VerbalEvaluation");

                    xmlS.startTag("", "Date");
                    xmlS.text(r.getDate());
                    xmlS.endTag("", "Date");

                    xmlS.startTag("", "Time");
                    xmlS.text(r.getTime());
                    xmlS.endTag("", "Time");

                    xmlS.startTag("", "Showable");
                    xmlS.text(r.getShowableString());
                    xmlS.endTag("", "Showable");

                    // Stop writing the review.
                    xmlS.endTag("", "Review");
                }

                // Stop writing the meal.
                xmlS.endTag("", "Meal");
            }

            xmlS.endTag("", "file");
            xmlS.endDocument();

            System.out.println("XML file was written.");
            System.out.println(" XML file: " + writer.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }
}
