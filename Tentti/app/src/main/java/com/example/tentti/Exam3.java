package com.example.tentti;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;


abstract class X {
    String name;
    int size;
    double prize;

    X(String n, int s, double p) {
        name = n;
        size = s;
        prize = p;

    }
    public String getContent(String uri) {
        URL url = null;
        try {
            url = new URL(uri);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(url.openStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String content = "";
        String inputLine = null;
        while(true) {
            try {
                if (!((inputLine = br.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            content += inputLine + "\n";
        }
        return content;
    }
}


class A extends X {
    double weight;

    A(String n, int s, double p, double w) {
        super(n, s, p);
        weight = w;
    }

}

class B extends A implements Serializable {
    double lenght;

    B(String n, int s, double p, double w, double l) {
        super(n, s, p, w);
        lenght = l;
    }

    float a = 3;

    public float getUltimateValue() {
        return --a;
    }
}

public class Exam3 {
    public static void main(String[] args) {
        //X x = new X("Foobar", 10, 5.5);

        B b = new B("BooBoo", 12, 9.9, 7.2, 555.6);

        System.out.print(b.getContent("https://noppa.lut.fi/"));

    }

}


