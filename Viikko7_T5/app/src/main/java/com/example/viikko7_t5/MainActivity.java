package com.example.viikko7_t5;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.viikko7_t5.R;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class MainActivity extends AppCompatActivity {

    TextView otsikkoTeksti;
    EditText tekstiKentta;
    Context context = null;
    EditText tiedostoNimi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        otsikkoTeksti = (TextView) findViewById(R.id.otsikkoTeksti);
        otsikkoTeksti.setText("Tekstieditori");
        tekstiKentta = (EditText) findViewById(R.id.tekstiKentta);
        tiedostoNimi = (EditText) findViewById(R.id.tiedostoNimi);
        context = MainActivity.this;

        /*
        tekstiKentta.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                tulostaTeksti(text);
                return true;
            }
        });
        */

    }

    public void readFile(View v) {
        try {
            InputStream ins = context.openFileInput(tiedostoNimi.getText().toString());

            BufferedReader br = new BufferedReader(new InputStreamReader(ins));
            String rivi;
            String s = "";

            while ((rivi = br.readLine()) != null) {
                s = s + rivi;
            }
            tekstiKentta.setText(s);
            ins.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void writeFile(View v) {
        try {
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput(tiedostoNimi.getText().toString(), Context.MODE_PRIVATE));
            ows.write(tekstiKentta.getText().toString());
            ows.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Tiedosto kirjoitettu.");
        }
    }

    }

    /*
    public void tulostaTeksti(View v) {
        System.out.println("Hello World!");

        String juttu = newText.getText().toString();
        text.setText(juttu);
    }

    public void nollaaTeksti(View v) {
        text.setText("Hello World!");
    }
    */
