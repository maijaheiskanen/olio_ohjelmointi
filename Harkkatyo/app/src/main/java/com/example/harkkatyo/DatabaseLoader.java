package com.example.harkkatyo;

// Loads information from the database and puts in the objects.

public class DatabaseLoader {
    private static boolean done = false;

    public static boolean loadFromDatabase() {
        if (!done) {
            System.out.println("Loaded content from database. Everything is on date.");
            University.updateRestaurantList();
            University.updateAdminRestaurantList();

            for (Restaurant r : University.getRestaurantList()) {
                r.updateMenulist();
                r.updateMealList();

                for (Meal m : r.getMealList()) {
                    m.updateReviewList();
                }
            }

            done = true;
        }
        return true;
    }
}
