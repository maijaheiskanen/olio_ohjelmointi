package com.example.harkkatyo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import java.io.File;
import java.util.ArrayList;

public class CreateTestData {
    private static boolean isDone = false;

    public static boolean doesUniversityExist(SQLiteDatabase db) {
        Cursor res = db.rawQuery("SELECT " + TableContract.UniversityEntry.COLUMN_UNIVERSITYID +
                        " FROM " + TableContract.UniversityEntry.TABLE_NAME + ";",
                null);

        System.out.println("Database contained university: " + res.moveToFirst());
        return res.moveToFirst();
    }

    public static void createTestData() {
        if (!isDone) {
            // Test data, maybe delete at some point or at least make this better (checks if already exists etc.)

            DatabaseInsert.insertUser(DatabaseInsert.getDatabase(), "User", "user", "user", "0");

            DatabaseInsert.insertUniversity(DatabaseInsert.getDatabase(), "LUT", "Yliopistonkatu 34");
            DatabaseInsert.insertRestaurant(DatabaseInsert.getDatabase(), 1, "Laseri", "Laserkatu 10");
            DatabaseInsert.insertRestaurant(DatabaseInsert.getDatabase(), 1, "YO-talo", "Laserkatu 6");

            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Nistipata", 1, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Sisäfileepihvi", 2, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Kukka- ja parsakaali", 3, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Makaronilaatikko", 1, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Hummeri", 2, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Salaatti", 3, "0", "0");

            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Kaurapuuro", 1, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Naudanpaisti", 2, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Soijarouhetta", 3, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Nakkikeitto", 1, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Porsaan kassleria", 2, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Kasvislasagne", 3, "0", "0");

            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Kookosbroilerihöystöä", 1, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Broileria", 2, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Pinaattiohukaiset", 3, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Nakkikeitto", 1, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Uunilohi", 2, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Parsakaalilaatikko", 3, "0", "0");

            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Kaalilaatikko", 1, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Lehtipihvi", 2, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 1, "Kasviksia", 3, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Kebabrisotto", 1, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Kananpojan filee", 2, "0", "0");
            DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), 2, "Kasvisgratiinia", 3, "0", "0");

            DatabaseInsert.insertReview(DatabaseInsert.getDatabase(), "1", 3, "Testijuttujaaaa.", "1");

            DatabaseInsert.insertUser(DatabaseInsert.getDatabase(), "Admin", "admin", "admin", "1");
            DatabaseInsert.insertMaintenance(DatabaseInsert.getDatabase(), "2", 1);
            DatabaseInsert.insertUser(DatabaseInsert.getDatabase(), "Admin2", "admin2", "admin2", "1");
            DatabaseInsert.insertMaintenance(DatabaseInsert.getDatabase(), "3", 2);

            DatabaseInsert.insertMenu(DatabaseInsert.getDatabase(), "1", "2019-07-27", "1", "2", "3");
            DatabaseInsert.insertMenu(DatabaseInsert.getDatabase(), "2", "2019-07-27", "4", "5", "6");

            DatabaseInsert.insertMenu(DatabaseInsert.getDatabase(), "1", "2019-07-28", "7", "8", "9");
            DatabaseInsert.insertMenu(DatabaseInsert.getDatabase(), "2", "2019-07-28", "10", "11", "12");

            DatabaseInsert.insertMenu(DatabaseInsert.getDatabase(), "1", "2019-07-29", "13", "14", "15");
            DatabaseInsert.insertMenu(DatabaseInsert.getDatabase(), "2", "2019-07-29", "16", "17", "18");

            DatabaseInsert.insertMenu(DatabaseInsert.getDatabase(), "1", "2019-07-30", "19", "20", "21");
            DatabaseInsert.insertMenu(DatabaseInsert.getDatabase(), "2", "2019-07-30", "22", "23", "24");

            ArrayList<Menu> menuList = DatabaseSearcher.searchMenus(DatabaseInsert.getDatabase());
            System.out.println("In CreateTestData class, after inserting all data, database contains following menus:");
            for (Menu m : menuList) {
                System.out.println(m.getMenuID() + " " + m.getRestaurantID() + " " + m.getDate() + " " + m.getKokinSuositus() + " " + m.getKokinSuositus() + " " + m.getKasvisherkkuja());
            }

            System.out.println("In CreateTestData class, after inserting all data, university contains following menus:");
            for (Restaurant r : University.getRestaurantList()) {
                for (Menu m : r.getMenuList()) {
                    System.out.println(m.getMenuID() + " " + m.getRestaurantID() + " " + m.getDate() + " " + m.getKokinSuositus() + " " + m.getKokinSuositus() + " " + m.getKasvisherkkuja());
                }
            }

            DatabaseInsert.insertUser(DatabaseInsert.getDatabase(), "Brian", "brian", "brian", "0");
            DatabaseInsert.insertUser(DatabaseInsert.getDatabase(), "Toveri", "toveri", "toveri", "0");
            DatabaseInsert.insertUser(DatabaseInsert.getDatabase(), "Student314", "student314", "student314", "0");
            DatabaseInsert.insertUser(DatabaseInsert.getDatabase(), "Critic", "critic", "critic", "0");


            DatabaseInsert.insertReview(DatabaseInsert.getDatabase(), "2", 2, "Didn't taste very much.", "4");
            DatabaseInsert.insertReview(DatabaseInsert.getDatabase(), "2", 4, "I liked it!!.", "5");
            DatabaseInsert.insertReview(DatabaseInsert.getDatabase(), "3", 5, "SUPER yummy!!!.", "6");
            DatabaseInsert.insertReview(DatabaseInsert.getDatabase(), "4", 2, "Too much salt...", "7");
            DatabaseInsert.insertReview(DatabaseInsert.getDatabase(), "5", 3, "Meh.", "4");
            DatabaseInsert.insertReview(DatabaseInsert.getDatabase(), "6", 1, "Couldn't eat!!.", "5");
            DatabaseInsert.insertReview(DatabaseInsert.getDatabase(), "7", 5, "Very good.", "1");



            isDone = true;
        }
    }
}
