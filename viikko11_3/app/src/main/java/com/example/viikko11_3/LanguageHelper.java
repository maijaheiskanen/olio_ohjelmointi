package com.example.viikko11_3;

import android.content.res.Configuration;
import android.content.res.Resources;

import java.util.Locale;

@SuppressWarnings("ALL")
public class LanguageHelper {

    public static void changeLocale(Resources res, String locale) {
        Configuration config;
        config = new Configuration(res.getConfiguration());

        switch (locale) {
            case "suomi":
                config.locale = new Locale("fi");
                break;
            case "English":
                config.locale = new Locale("en");
                break;
            case "svenska":
                config.locale = new Locale("sv");
                break;

        }
        res.updateConfiguration(config, res.getDisplayMetrics());
    }

}
