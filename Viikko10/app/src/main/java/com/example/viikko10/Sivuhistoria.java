package com.example.viikko10;

import java.util.ArrayList;

public class Sivuhistoria {
    private ArrayList<String> lista = new ArrayList<>();
    static private Sivuhistoria sh = new Sivuhistoria();

    private Sivuhistoria() {

    }

    static public Sivuhistoria getInstance() {
        return sh;
    }

    public void lisaaSivu(String url) {
        if (lista.size() >= 10) {
            lista.remove(lista.size()-1);
            System.out.println("Poistettu aikaisempi sivu sivuhistoriasta. Koko ylitti 10 sivua.");
        }
        try {
            if (!lista.get(0).equals(url)) {
                System.out.println("Listan edellinen ensimmäinen: " + lista.get(0));
                System.out.println("Listan uusi ensimmäinen: " + url);
                lista.add(0, url);
            }
        } catch (Exception e) {
            System.out.println("Lista oli tyhjä.");
            lista.add(0, url);
        }
    }

    public String getUrl(int i) {
        return lista.get(i);

    }


    public void poistaSivut(int indeksi) {
        if (indeksi == 0) {
            System.out.println("Ei poistettu sivuhistoriasta mitään.");
        } else {
            for (int i = 0; i < indeksi; i++) {
                System.out.println("Sivuhistoriasta poistettu: " + lista.get(0));
                lista.remove(0);
            }
        }
    }



}
