package com.example.harkkatyo;

// Contains the method to write a string in a given file.

import android.content.Context;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class FileWriter {

    public static boolean writeStringInFile(Context context, String filename, String s) {

        try {
            OutputStreamWriter writer = new OutputStreamWriter(context.openFileOutput(filename, Context.MODE_PRIVATE));
            writer.write(s);
            writer.close();
            System.out.println("XML file written.");
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
