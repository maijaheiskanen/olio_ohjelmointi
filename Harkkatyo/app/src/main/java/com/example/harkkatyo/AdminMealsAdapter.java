package com.example.harkkatyo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class AdminMealsAdapter extends RecyclerView.Adapter<AdminMealsAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<Meal> mMealList = new ArrayList<>();

    public AdminMealsAdapter(Context context, ArrayList<Meal> mealList) {
        mMealList = mealList;
        mContext = context;
    }

    @NonNull
    @Override
    public AdminMealsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_admin_meals_list_item, viewGroup, false);
        AdminMealsAdapter.ViewHolder holder = new AdminMealsAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AdminMealsAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.name.setText(mMealList.get(i).getName());
            if (mMealList.get(i).getType() == 1) {
                viewHolder.type.setText("Kotoisia makuja");
            } else if (mMealList.get(i).getType() == 2) {
                viewHolder.type.setText("Kokin suositus");
            } else if (mMealList.get(i).getType() == 3) {
                viewHolder.type.setText("Kasvisherkkuja");
            }
            viewHolder.restaurant.setText(mMealList.get(i).getRestaurantName());

            // Checks if meal belongs to admin and sets the deleteButton clickable based on that.
            // This is actually useless know when the mealList (parameter to constructor) contains only meals that belong to admin.
            ArrayList<String> adminMealList = DatabaseSearcher.searchMealStringsAdmin(DatabaseInsert.getDatabase(), CurrentUser.getUserID());
            if (!adminMealList.contains(mMealList.get(i).getId())) {
                viewHolder.deleteButton.setEnabled(false);
                viewHolder.deleteButton.setBackgroundColor(0x00fffff);
            }


            viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Setting the button not clickable after first click will prevent the app from crashing by trying to remove the same list item twice.
                viewHolder.deleteButton.setEnabled(false);

                // Saving the position of removable item.
                int position = viewHolder.getAdapterPosition();

                // Removing the item from database.
                DatabaseRemover.removeMeal(Integer.parseInt(mMealList.get(position).getId()));

                // Removing from restaurant object.
                University.getRestaurant(mMealList.get(position).getRestaurantID()).removeMealList(mMealList.get(position));

                // Removing the item from the recyclerview.
                removeItem(position);

                Toast.makeText(mContext, "Meal deleted!", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMealList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name, type, restaurant;
        private LinearLayout reviewListItemLayout;
        private Button deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.mealNameOutput);
            type = itemView.findViewById(R.id.mealTypeOutput);
            restaurant = itemView.findViewById(R.id.mealRestaurantOutput);
            deleteButton = itemView.findViewById(R.id.adminMealsDeleteButton);
            reviewListItemLayout = itemView.findViewById(R.id.admin_meals_list_item);
        }
    }

    private void removeItem(int position) {
        mMealList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mMealList.size());
    }

}