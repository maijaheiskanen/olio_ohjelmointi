package com.example.viikko10;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    WebView selain;
    EditText osoite;
    Button lataaSivu;
    int laskuri;
    Sivuhistoria sh = Sivuhistoria.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        selain = findViewById(R.id.selain);
        selain.setWebViewClient(new WebViewClient());
        selain.getSettings().setJavaScriptEnabled(true);

        osoite = findViewById(R.id.osoite);
        lataaSivu = findViewById(R.id.lataaSivuNappi);

        laskuri = 0;

    }



    /*
    public class YXWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            if (laskuri == 0) {
                if (osoite.getText().toString().equals("index.html")) {
                    sh.lisaaSivu("file:///android_asset/index.html");

                    System.out.println("Sivuhistoriaan lisätty vewviewclientissa: file:///android_asset/index.html");

                } else {
                    sh.lisaaSivu(request.getUrl().toString());
                    System.out.println("Sivuhistoriaan lisätty vewviewclientissa: " + request.getUrl().toString());
                }
            }
            return false;
        }
        /*
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.i("Listener", "Start");
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.i("Listener", "Finish");
            if (laskuri == 0) {
                sh.lisaaSivu(selain.getUrl());
                System.out.println("Sivuhistoriaan lisätty vewviewclientissa.");
            }
        }


    }*/



    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lataaSivuNappi:
                sh.poistaSivut(laskuri);
                laskuri = 0;
                System.out.println(osoite.getText());
                if (osoite.getText().toString().equals("index.html")) {
                    selain.loadUrl("file:///android_asset/index.html");
                    System.out.println("index.html ladattu.");
                } else {
                    selain.loadUrl("http://" + osoite.getText());
                    System.out.println("Sivu ladattu." + osoite.getText());
                }
                sh.lisaaSivu(selain.getUrl());
                System.out.println("Sivuhistoriaan lisätty onclikissä.");
                break;

            case R.id.paivitaSivuNappi:
                selain.loadUrl(selain.getUrl());
                System.out.println("Sivu päivitetty.");
                break;

            case R.id.shoutOutNappi:
                selain.evaluateJavascript("javascript:shoutOut()", null);
                System.out.println("shoutOut");
                break;

            case R.id.initializeNappi:
                selain.evaluateJavascript("javascript:initialize()", null);
                System.out.println("initialize");
                break;

            case R.id.seuraavaNappi:
                laskuri--;
                try {
                    selain.loadUrl(sh.getUrl(laskuri));
                } catch (Exception e) {
                    System.out.println("Sivua ei ollut.");
                    laskuri++;
                }

                break;

            case R.id.edellinenNappi:
                laskuri++;
                try {
                    selain.loadUrl(sh.getUrl(laskuri));
                } catch (Exception e) {
                    System.out.println("Sivua ei ollut.");
                    laskuri--;
                }
                break;

        }
    }

}
