package com.example.harkkatyo;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class activity_CreateAccount extends AppCompatActivity {
    private EditText nameInput, emailInput, passwordInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_activity);

        // Finding Views.
        nameInput = findViewById(R.id.createNameInput);
        emailInput = findViewById(R.id.createEmailInput);
        passwordInput = findViewById(R.id.createPasswordInput);

    }

    // Tried to create this in own class so that this method could be called from there but didn't succeed, so now I have this method in every class.
    private void loadActivity(Class where) {
        Intent intent = new Intent(this, where);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    // Notices if any button is clicked and depending on clicked button, makes things.
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpButton:
                // Saves new user in the database. Gets users name, email and password from EditText views. Sets user not admin.
                DatabaseInsert.insertUser(DatabaseInsert.getDatabase(), nameInput.getText().toString(), emailInput.getText().toString(), passwordInput.getText().toString(), "0");
                Toast.makeText(this, "An account created.",
                        Toast.LENGTH_LONG).show();
                loadActivity(MainActivity.class);
                break;
            case R.id.cancelButton:
                Toast.makeText(this, "Canceled.",
                        Toast.LENGTH_LONG).show();
                loadActivity(MainActivity.class);
                break;
        }

    }
}
