package com.example.harkkatyo;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

// This class contains the methods to add data in the database.
// Also contains the used SQLiteDatabase (why I do things like this, what's wrong with me).

// I use try-catch with some SQLite things, but those wont have exceptions so the try-catch is kinda useless.
// (If I understood it right, at least it didn't print any exception when I tried to debug)

public class DatabaseInsert {

    // For some reason I decided to put this database here. It's not the optimal situation but this is what it is. (Horrible load of work to change the method calls everywhere)
    private static SQLiteDatabase database = null;
    public static void setDatabase(SQLiteDatabase pDB) {
        database = pDB;
    }
    public static SQLiteDatabase getDatabase() {
        return database;
    }

    // Adding rows in databases tables.
    //TODO: Change hard-coded column names to come from TableContrat.java.

    // Didn't have time to do what the to-do comment above tells to to.

    public static boolean insertUser(SQLiteDatabase db, String name, String email, String password, String isAdmin) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("Name", name);
        contentValues.put("Email", email);
        contentValues.put("Password", password);
        contentValues.put("IsAdmin", isAdmin);

        try {
            if (db.insert(TableContract.UserEntry.TABLE_NAME, null, contentValues) > 0) {
                System.out.println("User saved in the database.");
                return true;
            } else {
                return false;
            }
        } catch (SQLiteConstraintException e){ //TODO: Catch real exceptions, not just general.
            System.out.println("Exception: " + e);
            return false;
        }
    }

    /* Didn't want this table anymore but not sure if never return it back on use.
    public static boolean insertAdmin(SQLiteDatabase db, String name, String email, String password) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("Name", name);
        contentValues.put("Email", email);
        contentValues.put("Password", password);

        try {
            db.insert(TableContract.AdminEntry.TABLE_NAME, null, contentValues);
            System.out.println("Admin saved in the database.");
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    */

    public static boolean insertMaintenance(SQLiteDatabase db, String userID, int restaurantID) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("userID", userID);
        contentValues.put("restaurantID", restaurantID);

        try {
            if (db.insert(TableContract.MaintenanceEntry.TABLE_NAME, null, contentValues) > 0) {
                System.out.println("Maintenance saved in the database.");
                return true;
            } else {
                return  false;
            }
        } catch (SQLiteConstraintException e){ //TODO: Catch real exceptions, not just general.
            System.out.println("Exception: " + e);
            return false;
        }
    }

    public static boolean insertReview(SQLiteDatabase db, String mealID, int grade, String verbal, String userID) {
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat time = new SimpleDateFormat("hh:mm:ss");

        Review review = DatabaseSearcher.searchReview(DatabaseInsert.getDatabase(), date.format(Calendar.getInstance().getTime()));

        if (review == null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("userID", userID);
            contentValues.put("mealID", mealID);
            contentValues.put("Grade", grade);
            contentValues.put("VerbalEvaluation", verbal);
            String sDate = date.format((Calendar.getInstance().getTime()));
            contentValues.put("Date", sDate);
            String sTime = time.format((Calendar.getInstance().getTime()));
            contentValues.put("Time", sTime);

            try {
                if (db.insert(TableContract.ReviewEntry.TABLE_NAME, null, contentValues) > 0) {
                    System.out.println("Review saved in the database.");
                    for (Restaurant r: University.getRestaurantList()) {
                        for (Meal m : r.getMealList()) {
                            if (m.getId().equals(mealID)) {
                                m.updateReviewList();
                            }
                        }
                    }
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) { //TODO: Catch real exceptions, not just general.
                System.out.println("While saving a new review in the database occurred following exception: " + e);
                return false;
            }
        } else {
            System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            return false;
        }
    }

    public static boolean insertUniversity(SQLiteDatabase db, String name, String address) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("Name", name);
        contentValues.put("Address", address);
        contentValues.put("universityID", "1");

        try {
            if (db.insert(TableContract.UniversityEntry.TABLE_NAME, null, contentValues) > 0) {
                System.out.println("University saved in the database.");
                return true;
            } else {
                return false;
            }
        } catch (SQLiteConstraintException e){ //TODO: Catch real exceptions, not just general.
            System.out.println("Exception: " + e);
            return false;
        }
    }

    public static boolean insertRestaurant(SQLiteDatabase db, int universityID, String name, String address) {

        ContentValues contentValues = new ContentValues();
        contentValues.put("universityID", universityID);
        contentValues.put("Name", name);
        contentValues.put("Address", address);


        try {
            if (db.insert(TableContract.RestaurantEntry.TABLE_NAME, null, contentValues) > 0) {
                System.out.println("Restaurant saved in the database.");
                University.updateRestaurantList();
                return true;
            } else {
                return false;
            }
        } catch (SQLiteConstraintException e){ //TODO: Catch real exceptions, not just general.
            System.out.println("Exception: " + e);
            return false;
        }
    }

    public static boolean insertMeal(SQLiteDatabase db, int restaurantID, String name, int type, String lactoseFree, String glutenFree) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("restaurantID", restaurantID);
        contentValues.put("Name", name);
        contentValues.put("Type", type);
        contentValues.put("LactoseFree", lactoseFree);
        contentValues.put("GlutenFree", glutenFree);

        try {
            if (db.insert(TableContract.MealEntry.TABLE_NAME, null, contentValues) > 0) {
                System.out.println("Meal " + name + " saved in the database.");
                for (Restaurant r : University.getRestaurantList()) {
                    if (r.getID().equals(restaurantID)) {
                        r.updateMealList();
                    }
                }
                return true;
            } else {
                System.out.println("SAVING MEAL IN THE DATABASE DIDN'T SUCCEED");
                return false;
            }
        } catch (SQLiteConstraintException e){ //TODO: Catch real exceptions, not just general.
            System.out.println("Exception: " + e);
            return false;
        }
    }

    public static boolean insertMenu(SQLiteDatabase db, String restaurantID, String date, String kotoisiaMakujaID, String kokinSuositusID, String kasvisherkkujaID) {
        String menuID = null;

        ContentValues contentValues = new ContentValues();
        contentValues.put("restaurantID", restaurantID);

        try {
            // Adding content to the Menu table.
            if (db.insert(TableContract.MenuEntry.TABLE_NAME, null, contentValues) < 1) {
                return false;
            }

            // Adding content to the HasDate table.
            contentValues = new ContentValues();

            Cursor res = db.rawQuery("SELECT " + TableContract.MenuEntry.COLUMN_MENUID + " FROM " + TableContract.MenuEntry.TABLE_NAME + ";", null);
            res.moveToNext();

            while (!res.isAfterLast()) {
                menuID = res.getString(res.getColumnIndex(TableContract.MenuEntry.COLUMN_MENUID));

                System.out.println("Found menuID: " + menuID);
                res.moveToNext();
            }

            // Inserting menu related data in the database and checking if inserting was successful,
            // if not, remove the menu which removes also content related to the menu from HasDate and OnList tables (cascade).

            if (!DatabaseInsert.insertHasDate(DatabaseInsert.getDatabase(), date, menuID)) {
                System.out.println("Inserting HasDate was unsuccessful.");
                if (!DatabaseRemover.removeMenu(Integer.parseInt(menuID))) {
                    System.out.println("Deleting the menu while inserting the menu was unsuccessful");
                }
                return false;
            }

            // Adding content to the OnList table.

            if (!DatabaseInsert.insertOnList(DatabaseInsert.getDatabase(), kotoisiaMakujaID, menuID)) {
                System.out.println("Inserting OnList was unsuccessful.");
                if (!DatabaseRemover.removeMenu(Integer.parseInt(menuID))) {
                    System.out.println("Deleting the menu while inserting the menu was unsuccessful");
                }
                return false;
            }

            // Adding content to the OnList table.

            if (!DatabaseInsert.insertOnList(DatabaseInsert.getDatabase(), kokinSuositusID, menuID)) {
                System.out.println("Inserting OnList was unsuccessful.");
                if (!DatabaseRemover.removeMenu(Integer.parseInt(menuID))) {
                    System.out.println("Deleting the menu while inserting the menu was unsuccessful");
                }
                return false;
            }

            // Adding content to the OnList table.

            if (!DatabaseInsert.insertOnList(DatabaseInsert.getDatabase(), kasvisherkkujaID, menuID)) {
                System.out.println("Inserting OnList was unsuccessful.");
                if (!DatabaseRemover.removeMenu(Integer.parseInt(menuID))) {
                    System.out.println("Deleting the menu while inserting the menu was unsuccessful");
                }
                return false;
            }

            for (Restaurant r : University.getRestaurantList()) {
                if (r.getID().equals(restaurantID)) {
                    r.updateMenulist();
                }
            }

            System.out.println("Menu saved in the database: " + restaurantID + " " + kotoisiaMakujaID + " " + kokinSuositusID + " " + kasvisherkkujaID);
            return true;
        } catch (SQLiteConstraintException E){ //TODO: Catch real exceptions, not just general.
            return false;
        }
    }

    /* Didn't want this table anymore but not sure if never return it back on use.
    public static boolean insertDate(SQLiteDatabase db, String date) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);

        try {
            db.insert(TableContract.DateEntry.TABLE_NAME, null, contentValues);
            System.out.println("Date saved in the database.");
            return true;
        } catch (Exception E){
            return false;
        }
    }
    */


    public static boolean insertHasDate(SQLiteDatabase db, String date, String menuID) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", date);
        contentValues.put("menuID", menuID);

        try {
            if (db.insert(TableContract.HasDateEntry.TABLE_NAME, null, contentValues) > 0) {
                System.out.println("HasDate " + date + " saved in the database.");
                return true;
            } else {
                return false;
            }
        } catch (SQLiteConstraintException e){ //TODO: Catch real exceptions, not just general.
            System.out.println("Exception: " + e);
            return false;
        }
    }


    public static boolean insertOnList(SQLiteDatabase db, String mealID, String menuID) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("mealID", mealID);
        contentValues.put("menuID", menuID);

        try {
            if (db.insert(TableContract.OnListEntry.TABLE_NAME, null, contentValues) > 0) {
                System.out.println("OnList saved in the database: " + "MenuID: " + menuID + " | MealID: " + mealID);
                return true;
            } else {
                return false;
            }
        } catch (SQLiteConstraintException e){ //TODO: Catch real exceptions, not just general.
            System.out.println("Exception: " + e);
            return false;
        }
    }
}
