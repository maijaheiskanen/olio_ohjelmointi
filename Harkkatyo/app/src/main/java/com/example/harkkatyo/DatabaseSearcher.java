package com.example.harkkatyo;

// This class contains SQLite Select-statements for every situation which might be good or just inefficient programming.
// Well, at least I have practised creating those statements.

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.TabLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DatabaseSearcher {

    // Used to check if given password matches given email when user is trying to log in.
    // Returns the password matching given email.
    public static String searchPasswordByEmail(SQLiteDatabase db, String email) {
        String password = null;

        Cursor res = db.rawQuery("SELECT " + TableContract.UserEntry.COLUMN_PASSWORD +
                " FROM " + TableContract.UserEntry.TABLE_NAME +
                " WHERE " + TableContract.UserEntry.COLUMN_EMAIL + " = \"" + email + "\"" + ";",
                null);
        res.moveToNext();

        int i = 0;
        while (!res.isAfterLast()) {
            System.out.println(i++);
            password = res.getString(res.getColumnIndex("Password"));
            res.moveToNext();
        }

        res.close();
        return password;
    }

    // For some reason (I forgot why) I wanted to save the users password in CurrentUser object (very safe I know).
    // This method returns that password.
    public static String searchPasswordByUserID(SQLiteDatabase db, String userID) {
        String password = null;

        Cursor res = db.rawQuery("SELECT " + TableContract.UserEntry.COLUMN_PASSWORD +
                        " FROM " + TableContract.UserEntry.TABLE_NAME +
                        " WHERE " + TableContract.UserEntry.COLUMN_USERID + " = \"" + userID + "\"" + ";",
                        null);
        res.moveToNext();

        int i = 0;
        // This while loop is actually kinda useless, could be removed.
        while (!res.isAfterLast()) {
            // Debugging thing.
            // System.out.println(i++);
            password = res.getString(res.getColumnIndex("Password"));
            res.moveToNext();
        }

        res.close();
        return password;
    }

    // Searches userID by given email and password. Used to save userID in CurrentUser object.
    // Returns userID as a String.
    public static String searchUserID(SQLiteDatabase db, String email, String password) {
        String userID = null;

        Cursor res = db.rawQuery("SELECT " + TableContract.UserEntry.COLUMN_USERID +
                        " FROM " + TableContract.UserEntry.TABLE_NAME +
                        " WHERE (" + TableContract.UserEntry.COLUMN_EMAIL + " = \"" + email + "\"" +
                        " AND " + TableContract.UserEntry.COLUMN_PASSWORD + " = \"" + password + "\");",
                        null);
        res.moveToNext();

        int i = 0;
        // This while loop is actually kinda useless, could be removed.
        while (!res.isAfterLast()) {
            // Debugging thing.
            // System.out.println(i++);
            userID = res.getString(res.getColumnIndex(TableContract.UserEntry.COLUMN_USERID));
            res.moveToNext();
        }
        res.close();

        return userID;
    }

    // Returns an User object containing information found by userID given as parameter.
    public static User searchUserInfo(SQLiteDatabase db, String pUserID) {
        String name = null;
        String email = null;
        String password = null;
        String isAdmin = null;

        Cursor res = db.rawQuery("SELECT " + TableContract.UserEntry.COLUMN_NAME + ", " +
                        TableContract.UserEntry.COLUMN_EMAIL + ", " +
                        TableContract.UserEntry.COLUMN_PASSWORD + ", " +
                        TableContract.UserEntry.COLUMN_ISADMIN +
                        " FROM " + TableContract.UserEntry.TABLE_NAME +
                        " WHERE " + TableContract.UserEntry.COLUMN_USERID + " = \"" + pUserID + "\"" + ";",
                        null);
        res.moveToNext();

        // This while loop is actually kinda useless, could be removed.
        while (!res.isAfterLast()) {
            // Debugging thing.
            // System.out.println(i++);
            name = res.getString(res.getColumnIndex("Name"));
            email = res.getString(res.getColumnIndex("Email"));
            password = res.getString(res.getColumnIndex("Password"));
            isAdmin = res.getString(res.getColumnIndex("IsAdmin"));
            res.moveToNext();
        }
        res.close();


        boolean isAdminBoolean;
        if (name == null || email == null) {
            return null;
        } else {
            if (isAdmin.equals("1")) {
                isAdminBoolean = true;
            } else {
                isAdminBoolean = false;
            }
            return new User(pUserID, name, email, password, isAdminBoolean);
        }
    }

    // Checks if given userID is admin.
    public static boolean searchIsAdmin(SQLiteDatabase db, String userID) {
        String isAdmin = null;

        Cursor res = db.rawQuery("SELECT " + TableContract.UserEntry.COLUMN_ISADMIN +
                        " FROM " + TableContract.UserEntry.TABLE_NAME +
                        " WHERE " + TableContract.UserEntry.COLUMN_USERID + " = \"" + userID + "\"" + ";",
                        null);
        res.moveToNext();

        // This while loop is kinda useless, could be removed.
        while (!res.isAfterLast()) {
            isAdmin = res.getString(res.getColumnIndex(TableContract.UserEntry.COLUMN_ISADMIN));
            res.moveToNext();
        }
        res.close();

        if (isAdmin.equals("0")) {
            return false;
        } else {
            return true;
        }
    }

    // Searches all reviews from the database and returns them in ArrayList.
    // Use while normal user (doesn't return reviews whose attribute showable is 0).
    public static ArrayList<Review> searchReviews(SQLiteDatabase db) {
        String reviewID = null;
        String userID = null;
        String userName = null;
        String meal = null;
        String mealID = null;
        String grade = null;
        String verbal = null;
        String date = null;
        ArrayList<Review> reviewList = new ArrayList<>();

        Cursor res = db.rawQuery("SELECT " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + ", " +
                        TableContract.ReviewEntry.COLUMN_REVIEWID + ", " +
                        TableContract.ReviewEntry.COLUMN_GARDE + ", " +
                        TableContract.ReviewEntry.COLUMN_DATE + ", " +
                        TableContract.ReviewEntry.COLUMN_VERBALEVALUATION + ", " +
                        TableContract.UserEntry.TABLE_NAME + "." + TableContract.UserEntry.COLUMN_NAME + " AS UserName, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS MealName, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID + " AS MealID" +
                        " FROM " + TableContract.ReviewEntry.TABLE_NAME +
                        " LEFT OUTER JOIN " + TableContract.MealEntry.TABLE_NAME +
                        " ON " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_MEALID + " = " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID +
                        " LEFT OUTER JOIN " + TableContract.UserEntry.TABLE_NAME +
                        " ON " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + " = " + TableContract.UserEntry.TABLE_NAME + "." + TableContract.UserEntry.COLUMN_USERID +
                        " WHERE " + TableContract.ReviewEntry.COLUMN_SHOWABLE + " = \"1\"" +
                        " ORDER BY " + TableContract.ReviewEntry.COLUMN_DATE + ";",
                        null);
        res.moveToNext();

        while (!res.isAfterLast()) {
            reviewID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_REVIEWID));
            userID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_USERID));
            userName = res.getString(res.getColumnIndex("UserName"));
            meal = res.getString(res.getColumnIndex("MealName"));
            mealID = res.getString(res.getColumnIndex("MealID"));
            grade = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_GARDE));
            verbal = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_VERBALEVALUATION));
            date = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_DATE));
            System.out.println(userName + meal + grade + verbal + date);
            reviewList.add(new Review(reviewID, userID, userName, meal, mealID, grade, verbal, date));
            res.moveToNext();
        }
        res.close();

        return reviewList;
    }

    // Used while finding the meals reviews for writing them to XML file.
    public static ArrayList<Review> searchReviewsByMealID(SQLiteDatabase db, String mealID) {
        String reviewID = null;
        String userID = null;
        String userName = null;
        String meal = null;
        String dMealID = null;
        String grade = null;
        String verbal = null;
        String date = null;
        String time;
        String showable;
        boolean showableBoolean = false;
        ArrayList<Review> reviewList = new ArrayList<>();

        Cursor res = db.rawQuery("SELECT " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + ", " +
                        TableContract.ReviewEntry.COLUMN_REVIEWID + ", " +
                        TableContract.ReviewEntry.COLUMN_GARDE + ", " +
                        TableContract.ReviewEntry.COLUMN_DATE + ", " +
                        TableContract.ReviewEntry.COLUMN_VERBALEVALUATION + ", " +
                        TableContract.ReviewEntry.COLUMN_SHOWABLE + ", " +
                        TableContract.ReviewEntry.COLUMN_TIME + ", " +
                        TableContract.UserEntry.TABLE_NAME + "." + TableContract.UserEntry.COLUMN_NAME + " AS UserName, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS MealName, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID + " AS MealID" +
                        " FROM " + TableContract.ReviewEntry.TABLE_NAME +
                        " LEFT OUTER JOIN " + TableContract.MealEntry.TABLE_NAME +
                        " ON " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_MEALID + " = " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID +
                        " LEFT OUTER JOIN " + TableContract.UserEntry.TABLE_NAME +
                        " ON " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + " = " + TableContract.UserEntry.TABLE_NAME + "." + TableContract.UserEntry.COLUMN_USERID +
                        " WHERE " +  TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_MEALID + " = \"" + mealID + "\"" + ";",
                null);
        res.moveToNext();

        while (!res.isAfterLast()) {
            reviewID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_REVIEWID));
            userID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_USERID));
            userName = res.getString(res.getColumnIndex("UserName"));
            meal = res.getString(res.getColumnIndex("MealName"));
            dMealID = res.getString(res.getColumnIndex("MealID"));
            grade = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_GARDE));
            verbal = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_VERBALEVALUATION));
            date = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_DATE));
            time = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_TIME));
            showable = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_SHOWABLE));

            // For some unknown reason I decided that it would be very very smart to have the showable variable as boolean
            // instead of String in the Review object and now I have to do this in too many places.
            if (showable.equals("1")) {
                showableBoolean = true;
            } else {
                showableBoolean = false;
            }

            // Debugging thing.
            //System.out.println(userName + meal + grade + verbal + date);
            reviewList.add(new Review(reviewID, userID, userName, meal, dMealID, grade, verbal, date, showableBoolean, time));
            res.moveToNext();
        }
        res.close();

        return reviewList;
    }

    // Takes admins restaurants as a parameter and choose only reviews belonging to those restaurants..
    public static ArrayList<Review> searchReviewsAdmin(SQLiteDatabase db, ArrayList<String> restaurantIdList) {
        String reviewID = null;
        String userID = null;
        String userName = null;
        String meal = null;
        String mealID = null;
        String grade = null;
        String verbal = null;
        String date = null;
        String showable = null;
        boolean booleanShowable = true;
        ArrayList<Review> reviewList = new ArrayList<>();

        String statement = "SELECT " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + ", " +
                TableContract.ReviewEntry.COLUMN_REVIEWID + ", " +
                TableContract.ReviewEntry.COLUMN_GARDE + ", " +
                TableContract.ReviewEntry.COLUMN_DATE + ", " +
                TableContract.ReviewEntry.COLUMN_VERBALEVALUATION + ", " +
                TableContract.UserEntry.TABLE_NAME + "." + TableContract.UserEntry.COLUMN_NAME + " AS UserName, " +
                TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS MealName" +", " +
                TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID + " AS MealID" +", " +
                TableContract.ReviewEntry.COLUMN_SHOWABLE +
                " FROM " + TableContract.ReviewEntry.TABLE_NAME +
                " LEFT OUTER JOIN " + TableContract.MealEntry.TABLE_NAME +
                " ON " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_MEALID + " = " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID +
                " LEFT OUTER JOIN " + TableContract.UserEntry.TABLE_NAME +
                " ON " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + " = " + TableContract.UserEntry.TABLE_NAME + "." + TableContract.UserEntry.COLUMN_USERID
                ;

        // Adding where statements to check if review belongs to restaurant that admin is responsible of.
        int i = 0;
        for (String s : restaurantIdList) {
            if (i == 0) {
                statement += " WHERE (" + TableContract.MealEntry.COLUMN_RESTAURANTID + " = \"" + s + "\""; // Replaced "AND" as "WHERE", don't why worked before but is working also now.
            }
            if (i >= 1) {
                statement += " OR " + TableContract.MealEntry.COLUMN_RESTAURANTID + " = \"" + s + "\"";
            }
            i++;
        }
        if (restaurantIdList.size() >= 1) {
            statement += ")";
        }

        Cursor res = db.rawQuery(statement + ";",
                null);
        res.moveToNext();

        while (!res.isAfterLast()) {
            reviewID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_REVIEWID));
            userID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_USERID));
            showable = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_SHOWABLE));

            if (showable.equals("1")) {
                booleanShowable = true;
            } else if (showable.equals("0")) {
                booleanShowable = false;
            }

            userName = res.getString(res.getColumnIndex("UserName"));
            meal = res.getString(res.getColumnIndex("MealName"));
            mealID = res.getString(res.getColumnIndex("MealID"));
            grade = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_GARDE));
            verbal = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_VERBALEVALUATION));
            date = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_DATE));
            System.out.println(userName + meal + grade + verbal + date);
            reviewList.add(new Review(reviewID, userID, userName, meal, mealID, grade, verbal, date, booleanShowable));
            res.moveToNext();
        }
        res.close();

        return reviewList;
    }

    // Checks admins restaurants and userIDs and then choose only those that fit given clauses.
    // Used in admins view where admin can see users reviews and basic information.
    public static ArrayList<Review> searchReviewsByUserID(SQLiteDatabase db, String pUserID) {
        String reviewID = null;
        String userID = null;
        String userName = null;
        String meal = null;
        String mealID = null;
        String grade = null;
        String verbal = null;
        String date = null;
        String showable = null;
        boolean booleanShowable = true;
        ArrayList<Review> reviewList = new ArrayList<>();

        String statement = "SELECT " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + ", " +
                TableContract.ReviewEntry.COLUMN_REVIEWID + ", " +
                TableContract.ReviewEntry.COLUMN_GARDE + ", " +
                TableContract.ReviewEntry.COLUMN_DATE + ", " +
                TableContract.ReviewEntry.COLUMN_VERBALEVALUATION + ", " +
                TableContract.UserEntry.TABLE_NAME + "." + TableContract.UserEntry.COLUMN_NAME + " AS UserName, " +
                TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS MealName" +", " +
                TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID + " AS MealID" +", " +
                TableContract.ReviewEntry.COLUMN_SHOWABLE +
                " FROM " + TableContract.ReviewEntry.TABLE_NAME +
                " LEFT OUTER JOIN " + TableContract.MealEntry.TABLE_NAME +
                " ON " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_MEALID + " = " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID +
                " LEFT OUTER JOIN " + TableContract.UserEntry.TABLE_NAME +
                " ON " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + " = " + TableContract.UserEntry.TABLE_NAME + "." + TableContract.UserEntry.COLUMN_USERID +
                " WHERE " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + " = \"" + pUserID + "\" ";

        Cursor res = db.rawQuery(statement + ";",
                null);
        res.moveToNext();

        while (!res.isAfterLast()) {
            reviewID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_REVIEWID));
            userID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_USERID));
            showable = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_SHOWABLE));

            if (showable.equals("1")) {
                booleanShowable = true;
            } else if (showable.equals("0")) {
                booleanShowable = false;
            }

            userName = res.getString(res.getColumnIndex("UserName"));
            meal = res.getString(res.getColumnIndex("MealName"));
            mealID = res.getString(res.getColumnIndex("MealID"));
            grade = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_GARDE));
            verbal = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_VERBALEVALUATION));
            date = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_DATE));
            System.out.println(userName + meal + grade + verbal + date);
            reviewList.add(new Review(reviewID, userID, userName, meal, mealID, grade, verbal, date, booleanShowable));
            res.moveToNext();
        }
        res.close();

        return reviewList;
    }

    // Checks admins restaurants and userIDs and then choose only those that fit given clauses.
    // Used in admins view where admin can see users reviews and basic information.
    public static ArrayList<Review> searchReviewsByAdminID(SQLiteDatabase db, String pUserID, ArrayList<String> restaurantIdList) {
        String reviewID = null;
        String userID = null;
        String userName = null;
        String meal = null;
        String mealID = null;
        String grade = null;
        String verbal = null;
        String date = null;
        String showable = null;
        boolean booleanShowable = true;
        ArrayList<Review> reviewList = new ArrayList<>();

        String statement = "SELECT " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + ", " +
                TableContract.ReviewEntry.COLUMN_REVIEWID + ", " +
                TableContract.ReviewEntry.COLUMN_GARDE + ", " +
                TableContract.ReviewEntry.COLUMN_DATE + ", " +
                TableContract.ReviewEntry.COLUMN_VERBALEVALUATION + ", " +
                TableContract.UserEntry.TABLE_NAME + "." + TableContract.UserEntry.COLUMN_NAME + " AS UserName, " +
                TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS MealName" +", " +
                TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID + " AS MealID" +", " +
                TableContract.ReviewEntry.COLUMN_SHOWABLE +
                " FROM " + TableContract.ReviewEntry.TABLE_NAME +
                " LEFT OUTER JOIN " + TableContract.MealEntry.TABLE_NAME +
                " ON " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_MEALID + " = " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID +
                " LEFT OUTER JOIN " + TableContract.UserEntry.TABLE_NAME +
                " ON " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + " = " + TableContract.UserEntry.TABLE_NAME + "." + TableContract.UserEntry.COLUMN_USERID +
                " WHERE " + TableContract.ReviewEntry.TABLE_NAME + "." + TableContract.ReviewEntry.COLUMN_USERID + " = \"" + pUserID + "\" ";

        // Adding where statements to check if review belongs to restaurant that admin is responsible of.
        int i = 0;
        for (String s : restaurantIdList) {
            if (i == 0) {
                statement += " AND (" + TableContract.MealEntry.COLUMN_RESTAURANTID + " = \"" + s + "\"";
            }
            if (i >= 1) {
                statement += " OR " + TableContract.MealEntry.COLUMN_RESTAURANTID + " = \"" + s + "\"";
            }
            i++;
        }

        if (restaurantIdList.size() >= 1) {
            statement += ")";
        }

        Cursor res = db.rawQuery(statement + ";",
                null);
        res.moveToNext();

        while (!res.isAfterLast()) {
            reviewID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_REVIEWID));
            userID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_USERID));
            showable = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_SHOWABLE));

            if (showable.equals("1")) {
                booleanShowable = true;
            } else if (showable.equals("0")) {
                booleanShowable = false;
            }

            userName = res.getString(res.getColumnIndex("UserName"));
            meal = res.getString(res.getColumnIndex("MealName"));
            mealID= res.getString(res.getColumnIndex("MealID"));
            grade = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_GARDE));
            verbal = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_VERBALEVALUATION));
            date = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_DATE));
            System.out.println(userName + meal + grade + verbal + date);
            reviewList.add(new Review(reviewID, userID, userName, meal, mealID, grade, verbal, date, booleanShowable));
            res.moveToNext();
        }
        res.close();

        return reviewList;
    }

    // Returns ArrayList containing existing meals.
    public static ArrayList<Meal> searchMeals(SQLiteDatabase db) {
        String meal = null, id = null, restaurantID = null, restaurantName = null;
        int type = -1;
        ArrayList<Meal> mealList = new ArrayList<>();

        Cursor res = db.rawQuery("SELECT " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS mealName, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID + " AS mealID, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_TYPE + ", " +
                        TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_NAME + " AS restaurantName, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_RESTAURANTID + " AS restaurantID " +
                        " FROM " + TableContract.MealEntry.TABLE_NAME +
                        " LEFT OUTER JOIN " + TableContract.RestaurantEntry.TABLE_NAME +
                        " ON " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_RESTAURANTID + " = " + TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + ";"
                , null);
        res.moveToNext();

        while (!res.isAfterLast()) {
            meal = res.getString(res.getColumnIndex("mealName"));
            type = Integer.parseInt(res.getString(res.getColumnIndex(TableContract.MealEntry.COLUMN_TYPE)));
            id = res.getString(res.getColumnIndex("mealID"));
            restaurantID = res.getString(res.getColumnIndex("restaurantID"));
            restaurantName = res.getString(res.getColumnIndex("restaurantName"));
            mealList.add(new Meal(meal, type, id, restaurantID, restaurantName));
            res.moveToNext();

        }
        res.close();

        return mealList;
    }

    // Returns ArrayList containing existing meals.
    public static Meal searchLastMeal(SQLiteDatabase db) {
        String meal = null, id = null, restaurantID = null, restaurantName = null;
        int type = -1;
        ArrayList<Meal> mealList = new ArrayList<>();

        Cursor res = db.rawQuery("SELECT " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS mealName, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID + " AS mealID, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_TYPE + ", " +
                        TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_NAME + " AS restaurantName, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_RESTAURANTID + " AS restaurantID " +
                        " FROM " + TableContract.MealEntry.TABLE_NAME +
                        " LEFT OUTER JOIN " + TableContract.RestaurantEntry.TABLE_NAME +
                        " ON " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_RESTAURANTID + " = " + TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + ";"
                , null);
        res.moveToNext();

        while (!res.isAfterLast()) {
            meal = res.getString(res.getColumnIndex("mealName"));
            type = Integer.parseInt(res.getString(res.getColumnIndex(TableContract.MealEntry.COLUMN_TYPE)));
            id = res.getString(res.getColumnIndex("mealID"));
            restaurantID = res.getString(res.getColumnIndex("restaurantID"));
            restaurantName = res.getString(res.getColumnIndex("restaurantName"));
            res.moveToNext();

        }
        Meal returnable = new Meal(meal, type, id, restaurantID, restaurantName);
        res.close();

        return returnable;
    }

    // Searches meals that belong to restaurants that admin controlls and returns meals in ArrayList.
    public static ArrayList<Meal> searchMealsAdmin(SQLiteDatabase db, String adminID) {
        String meal = null, id = null, restaurantID = null, restaurantName = null;
        int type = -1;
        ArrayList<Meal> mealList = new ArrayList<>();

        Cursor res = db.rawQuery("SELECT " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS mealName, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID + " AS mealID, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_TYPE + ", " +
                        TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_NAME + " AS restaurantName, " +
                        TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + " AS restaurantID " +
                        " FROM " + TableContract.MaintenanceEntry.TABLE_NAME +
                        " INNER JOIN " + TableContract.RestaurantEntry.TABLE_NAME +
                        " ON " + TableContract.MaintenanceEntry.TABLE_NAME + "." + TableContract.MaintenanceEntry.COLUMN_RESTAURANTID + " = " + TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID +
                        " INNER JOIN " + TableContract.MealEntry.TABLE_NAME +
                        " ON " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_RESTAURANTID + " = " + TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID +
                        " WHERE " + TableContract.MaintenanceEntry.TABLE_NAME + "." + TableContract.MaintenanceEntry.COLUMN_USERID + " = \"" + adminID + "\"" + ";" ,
                        null);
        res.moveToNext();

        while (!res.isAfterLast()) {
            meal = res.getString(res.getColumnIndex("mealName"));
            type = Integer.parseInt(res.getString(res.getColumnIndex(TableContract.MealEntry.COLUMN_TYPE)));
            id = res.getString(res.getColumnIndex("mealID"));
            restaurantID = res.getString(res.getColumnIndex("restaurantID"));
            restaurantName = res.getString(res.getColumnIndex("restaurantName"));
            mealList.add(new Meal(meal, type, id, restaurantID, restaurantName));
            res.moveToNext();

        }
        res.close();

        return mealList;
    }

    // Search meals that belong to restaurants an admin controls and returns them as Strings in ArrayList.
    public static ArrayList<String> searchMealStringsAdmin(SQLiteDatabase db, String adminID) {
        ArrayList<String> mealList = new ArrayList<>();

        Cursor res = db.rawQuery("SELECT " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS mealName, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID + " AS mealID, " +
                        TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_TYPE + ", " +
                        TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_NAME + " AS restaurantName, " +
                        TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + " AS restaurantID " +
                        " FROM " + TableContract.MaintenanceEntry.TABLE_NAME +
                        " INNER JOIN " + TableContract.RestaurantEntry.TABLE_NAME +
                        " ON " + TableContract.MaintenanceEntry.TABLE_NAME + "." + TableContract.MaintenanceEntry.COLUMN_RESTAURANTID + " = " + TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID +
                        " INNER JOIN " + TableContract.MealEntry.TABLE_NAME +
                        " ON " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_RESTAURANTID + " = " + TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID +
                        " WHERE " + TableContract.MaintenanceEntry.TABLE_NAME + "." + TableContract.MaintenanceEntry.COLUMN_USERID + " = \"" + adminID + "\"" + ";" ,
                null);
        res.moveToNext();

        while (!res.isAfterLast()) {
            mealList.add(res.getString(res.getColumnIndex("mealID")));
            res.moveToNext();

        }
        res.close();

        return mealList;
    }

    // Searches menus for users menus view.
    public static ArrayList<Menu> searchMenus(SQLiteDatabase db) {
        String restaurantID, restaurant, date, meal, type, menuID;
        ArrayList<Menu> menuList = new ArrayList<>();
        Menu tempMenu = null;
        boolean found = false;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = Calendar.getInstance().getTime();
        String stringCurrentDate = dateFormat.format(currentDate);

        Cursor res = db.rawQuery("SELECT " + TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_NAME + " AS restaurantName, " +
                TableContract.HasDateEntry.COLUMN_DATE + ", " +
                TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + " AS restaurantID" + ", " +
                TableContract.MenuEntry.TABLE_NAME + "." + TableContract.MenuEntry.COLUMN_MENUID+ " AS menuID, " +
                TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS mealName, " +
                TableContract.MealEntry.COLUMN_TYPE +
                " FROM " + TableContract.MenuEntry.TABLE_NAME +
                " INNER JOIN " + TableContract.OnListEntry.TABLE_NAME +
                " ON " + TableContract.MenuEntry.TABLE_NAME + "." + TableContract.MenuEntry.COLUMN_MENUID + " = " + TableContract.OnListEntry.TABLE_NAME + "." + TableContract.OnListEntry.COLUMN_MENUID +
                " INNER JOIN " + TableContract.MealEntry.TABLE_NAME +
                " ON " + TableContract.OnListEntry.TABLE_NAME + "." + TableContract.OnListEntry.COLUMN_MEALID + " = " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID +
                " INNER JOIN " + TableContract.HasDateEntry.TABLE_NAME +
                " ON " + TableContract.MenuEntry.TABLE_NAME + "." + TableContract.MenuEntry.COLUMN_MENUID + " = " + TableContract.HasDateEntry.TABLE_NAME + "." + TableContract.HasDateEntry.COLUMN_MENUID +
                " INNER JOIN " + TableContract.RestaurantEntry.TABLE_NAME +
                " ON " + TableContract.MenuEntry.TABLE_NAME + "." + TableContract.MenuEntry.COLUMN_RESTAURANTID + " = " + TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID +
                " WHERE " + TableContract.RestaurantEntry.COLUMN_AVAILABLE + " = \"" + "1\" AND " +
                TableContract.HasDateEntry.COLUMN_DATE + " >= \"" + stringCurrentDate + "\"" +
                "ORDER BY " + TableContract.HasDateEntry.COLUMN_DATE + // Here might be error, because I did stuff, don't know for sure tho.
                ";", null);
        res.moveToNext();

/*
        Cursor res = db.rawQuery("SELECT " + TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_NAME + " AS restaurantName, " +
                TableContract.HasDateEntry.COLUMN_DATE + ", " +
                TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + " AS restaurantID" + ", " +
                TableContract.MenuEntry.TABLE_NAME + "." + TableContract.MenuEntry.COLUMN_MENUID+ " AS menuID, " +
                TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_NAME + " AS mealName, " +
                TableContract.MealEntry.COLUMN_TYPE +
                " FROM " + TableContract.OnListEntry.TABLE_NAME +
                " INNER JOIN " + TableContract.MealEntry.TABLE_NAME +
                " ON " + TableContract.OnListEntry.TABLE_NAME + "." + TableContract.OnListEntry.COLUMN_MEALID + " = " + TableContract.MealEntry.TABLE_NAME + "." + TableContract.MealEntry.COLUMN_MEALID +
                " INNER JOIN " + TableContract.MenuEntry.TABLE_NAME +
                " ON " + TableContract.MenuEntry.TABLE_NAME + "." + TableContract.MenuEntry.COLUMN_MENUID + " = " + TableContract.OnListEntry.TABLE_NAME + "." + TableContract.OnListEntry.COLUMN_MENUID +
                " INNER JOIN " + TableContract.HasDateEntry.TABLE_NAME +
                " ON " + TableContract.MenuEntry.TABLE_NAME + "." + TableContract.MenuEntry.COLUMN_MENUID + " = " + TableContract.HasDateEntry.TABLE_NAME + "." + TableContract.HasDateEntry.COLUMN_MENUID +
                " INNER JOIN " + TableContract.RestaurantEntry.TABLE_NAME +
                " ON " + TableContract.MenuEntry.TABLE_NAME + "." + TableContract.MenuEntry.COLUMN_RESTAURANTID + " = " + TableContract.RestaurantEntry.TABLE_NAME + "." + TableContract.RestaurantEntry.COLUMN_RESTAURANTID +
                " WHERE " + TableContract.RestaurantEntry.COLUMN_AVAILABLE + " = \"" + "1\" AND " +
                TableContract.HasDateEntry.COLUMN_DATE + " >= \"" + stringCurrentDate + "\"" +
                "ORDER BY " + TableContract.HasDateEntry.COLUMN_DATE + // Here might be error, because I did stuff, don't know for sure tho.
                ";", null);
        res.moveToNext();
        */

        // Gets the found information.
        while (!res.isAfterLast()) {
            restaurantID = res.getString(res.getColumnIndex("restaurantID"));
            restaurant = res.getString(res.getColumnIndex("restaurantName"));
            date = res.getString(res.getColumnIndex(TableContract.HasDateEntry.COLUMN_DATE));
            meal = res.getString(res.getColumnIndex("mealName"));
            menuID = res.getString(res.getColumnIndex("menuID"));
            type = res.getString(res.getColumnIndex(TableContract.MealEntry.COLUMN_TYPE));

            System.out.println("In DatabaseSearcher.searchMenus found menu: " + restaurantID + " " + restaurant + " " + date + " " + meal + " " + menuID + " " + type);

            // Checks if the method has not found any menus yet and if true, creates a new one where adds found meal.
            if (menuList.size() != 0) {
                // Goes through the list and checks if a menu with founded menus date and restaurant already existing.
                for (Menu m : menuList) {
                    // If menu exist, add meal there.
                    if (m.getMenuID().equals(menuID)) {
                        found = true;
                        System.out.println("Adding meal to an old menu.");
                        if (type.equals("1")) {
                            m.setKotoisiaMakuja(meal);
                        } else if (type.equals("2")) {
                            m.setKokinSuositus(meal);
                        } else if (type.equals("3")) {
                            m.setKasvisherkkuja(meal);
                        } else {
                            System.out.println("Meal type wasn't 1, 2 or 3.");
                        }
                        continue;
                    }
                }
            }

            // If menu doesn't exist, create new menu and add meal there.
            if (!found) {
                System.out.println("Adding meal to a new menu.");
                tempMenu = new Menu(menuID, restaurantID, restaurant, date);

                if (tempMenu != null) {
                    if (type.equals("1")) {
                        tempMenu.setKotoisiaMakuja(meal);
                    } else if (type.equals("2")) {
                        tempMenu.setKokinSuositus(meal);
                    } else if (type.equals("3")) {
                        tempMenu.setKasvisherkkuja(meal);
                    } else {
                        System.out.println("Meal type wasn't 1, 2 or 3.");
                    }
                    menuList.add(tempMenu);
                    System.out.println("Tempmenu was: " + tempMenu.getMenuID() + " " + tempMenu.getRestaurantID() + " " + tempMenu.getKotoisiaMakuja() + " " + tempMenu.getKokinSuositus() + " " + tempMenu.getKasvisherkkuja() + " ");
                }
            }

            found = false;
            tempMenu = null;
            res.moveToNext();
        }
        res.close();

        return menuList;
    }

    // Used to check if review already exist with current user and date, and also used to get wanted review for editing it.
    // Note: the method to be more clear, current user could be taken as a parameter.
    public static Review searchReview(SQLiteDatabase db, String pDate) {
        String reviewID = null;
        String userID = null;
        String userName = null;
        String meal = null;
        String mealID = null;
        String grade = null;
        String verbal = null;
        String date = null;

        Review review = null;

        try {

            Cursor res = db.rawQuery("SELECT " + TableContract.ReviewEntry.COLUMN_REVIEWID + ", " +
                            TableContract.ReviewEntry.COLUMN_GARDE + ", " +
                            TableContract.ReviewEntry.COLUMN_DATE + ", " +
                            TableContract.ReviewEntry.COLUMN_MEALID + ", " +
                            TableContract.ReviewEntry.COLUMN_VERBALEVALUATION +
                            " FROM " + TableContract.ReviewEntry.TABLE_NAME +
                            " WHERE " + TableContract.ReviewEntry.COLUMN_USERID + " = \"" + CurrentUser.getUserID() + "\" AND " +
                            TableContract.ReviewEntry.COLUMN_DATE + " = \"" + pDate + "\";",
                    null);
            res.moveToNext();

            while (!res.isAfterLast()) {
                reviewID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_REVIEWID));
                grade = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_GARDE));
                mealID = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_MEALID));
                verbal = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_VERBALEVALUATION));
                date = res.getString(res.getColumnIndex(TableContract.ReviewEntry.COLUMN_DATE));
                review = new Review(reviewID, userID, userName, meal, mealID, grade, verbal, date);
                res.moveToNext();
            }
            res.close();
            if (review.getReviewID() == null) {
                return null;
            } else {
                return review;
            }
        } catch (Exception e) {
            System.out.println("Something went wrong while searching review for editing.");
            return null;
        }
    }

    // Searches all users and returns them as User objects in ArrayList.
    public static ArrayList<User> searchUsers(SQLiteDatabase db) {
        ArrayList<User> userList = new ArrayList<>();
        String userID =  null, name = null, email = null;

        try {

            Cursor res = db.rawQuery("SELECT " + TableContract.UserEntry.COLUMN_USERID + ", " +
                    TableContract.UserEntry.COLUMN_NAME + ", " +
                    TableContract.UserEntry.COLUMN_EMAIL +
                    " FROM " + TableContract.UserEntry.TABLE_NAME +
                    " ORDER BY " + TableContract.UserEntry.COLUMN_USERID + "",
                    null);
            res.moveToNext();

            while (!res.isAfterLast()) {
                userID = res.getString(res.getColumnIndex(TableContract.UserEntry.COLUMN_USERID));
                name = res.getString(res.getColumnIndex(TableContract.UserEntry.COLUMN_NAME));
                email = res.getString(res.getColumnIndex(TableContract.UserEntry.COLUMN_EMAIL));
                userList.add(new User(userID, name, email));
                res.moveToNext();
            }
            res.close();
        } catch (Exception e) {
            System.out.println("Exception while searching user list for admin was: " + e);
        }
        return userList;
    }


    public static ArrayList<String> searchAdminsRestaurants(SQLiteDatabase db, String adminID) {
        ArrayList<String> restaurantIdList = new ArrayList<>();

        try {

            Cursor res = db.rawQuery("SELECT " + TableContract.MaintenanceEntry.COLUMN_RESTAURANTID +
                    " FROM " + TableContract.MaintenanceEntry.TABLE_NAME +
                    " WHERE " + TableContract.MaintenanceEntry.COLUMN_USERID + " = \"" + adminID + "\"" + ";",
                    null);
            res.moveToNext();

            while (!res.isAfterLast()) {
                restaurantIdList.add(res.getString(res.getColumnIndex(TableContract.MaintenanceEntry.COLUMN_RESTAURANTID)));
                res.moveToNext();
            }
            res.close();

            return restaurantIdList;
        } catch (Exception e) {
            System.out.println("While searching restaurantIDs found exception: " + e);
        }
        return restaurantIdList;
    }

    // Searches restaurants for University.class
    public static ArrayList<Restaurant> searchRestaurants(SQLiteDatabase db, String universityID, String universityName) {
        ArrayList<Restaurant> restaurantList = new ArrayList<>();
        String restaurantID, name, address, available;
        boolean availableBoolean;

        try {

            Cursor res = db.rawQuery("SELECT " + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + ", " +
                            TableContract.RestaurantEntry.COLUMN_NAME + ", " +
                            TableContract.RestaurantEntry.COLUMN_ADDRESS + ", " +
                            TableContract.RestaurantEntry.COLUMN_AVAILABLE +
                            " FROM " + TableContract.RestaurantEntry.TABLE_NAME +
                            " WHERE " + TableContract.RestaurantEntry.COLUMN_UNIVERSITYID + " = \"" + universityID + "\"" + ";",
                    null);
            res.moveToNext();

            while (!res.isAfterLast()) {
                restaurantID = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_RESTAURANTID));
                name = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_NAME));
                address = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_ADDRESS));
                available = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_AVAILABLE));

                if (available.equals("1")) {
                    availableBoolean = true;
                } else {
                    availableBoolean = false;
                }

                restaurantList.add(new Restaurant(restaurantID, name, address, universityName, universityID, availableBoolean));
                res.moveToNext();
            }
            res.close();

            return restaurantList;
        } catch (Exception e) {
            System.out.println("While searching restaurantIDs found exception: " + e);
        }
        return restaurantList;
    }

    // Searches the latest added restaurant.
    public static Restaurant searchLastRestaurant(SQLiteDatabase db, String universityID, String universityName) {
        ArrayList<Restaurant> restaurantList = new ArrayList<>();
        String restaurantID = null, name = null, address = null, available = null;
        boolean availableBoolean = false;

        try {

            Cursor res = db.rawQuery("SELECT " + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + ", " +
                            TableContract.RestaurantEntry.COLUMN_NAME + ", " +
                            TableContract.RestaurantEntry.COLUMN_ADDRESS + ", " +
                            TableContract.RestaurantEntry.COLUMN_AVAILABLE +
                            " FROM " + TableContract.RestaurantEntry.TABLE_NAME +
                            " WHERE " + TableContract.RestaurantEntry.COLUMN_UNIVERSITYID + " = \"" + universityID + "\"" + ";",
                    null);
            res.moveToNext();

            while (!res.isAfterLast()) {
                restaurantID = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_RESTAURANTID));
                name = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_NAME));
                address = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_ADDRESS));
                available = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_AVAILABLE));

                if (available.equals("1")) {
                    availableBoolean = true;
                } else {
                    availableBoolean = false;
                }

                res.moveToNext();
            }
            res.close();

            return new Restaurant(restaurantID, name, address, universityName, universityID, availableBoolean);
        } catch (Exception e) {
            System.out.println("While searching restaurant found exception: " + e);
        }
        return null;
    }

    // Searches the most recently added restaurant (used in admins restaurants view to add maintenance to right restaurant)
    public static String searchLastRestaurantID(SQLiteDatabase db) {
        String restaurantID = null;

        try {

            Cursor res = db.rawQuery("SELECT " + TableContract.RestaurantEntry.COLUMN_RESTAURANTID +
                            " FROM " + TableContract.RestaurantEntry.TABLE_NAME,
                    null);
            res.moveToNext();

            while (!res.isAfterLast()) {
                restaurantID = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_RESTAURANTID));
                res.moveToNext();
            }
            res.close();

            return restaurantID;
        } catch (Exception e) {
            System.out.println("While searching restaurantIDs found exception: " + e);
        }
        return restaurantID;
    }

    // Searches a restaurant by restaurantID
    public static Restaurant searchRestaurantByID(SQLiteDatabase db, String pRestaurantID) {
        String restaurantID, name, address, available;
        boolean availableBoolean;

        try {

            Cursor res = db.rawQuery("SELECT " + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + ", " +
                            TableContract.RestaurantEntry.COLUMN_NAME + ", " +
                            TableContract.RestaurantEntry.COLUMN_ADDRESS + ", " +
                            TableContract.RestaurantEntry.COLUMN_AVAILABLE +
                            " FROM " + TableContract.RestaurantEntry.TABLE_NAME +
                            " WHERE " + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + " = \"" + pRestaurantID + "\"" + ";",
                    null);
            res.moveToNext();


            restaurantID = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_RESTAURANTID));
            name = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_NAME));
            address = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_ADDRESS));
            available = res.getString(res.getColumnIndex(TableContract.RestaurantEntry.COLUMN_AVAILABLE));

            if (available.equals("1")) {
                availableBoolean = true;
            } else {
                availableBoolean = false;
            }
            res.close();

            return new Restaurant(restaurantID, name, address, availableBoolean);

        } catch (Exception e) {
            System.out.println("While searching restaurantIDs found exception: " + e);
            return null;
        }
    }

    // Sets University classes information.
    public static void searchAndSetUniversity(SQLiteDatabase db) {

        try {
            Cursor res = db.rawQuery("SELECT " + TableContract.UniversityEntry.COLUMN_UNIVERSITYID + "," +
                    TableContract.UniversityEntry.COLUMN_NAME + "," +
                    TableContract.UniversityEntry.COLUMN_ADDRESS +
                    " FROM " + TableContract.UniversityEntry.TABLE_NAME + ";",
                    null);
            res.moveToNext();

            while(!res.isAfterLast()) {
                University.setUniversityID(res.getString(res.getColumnIndex(TableContract.UniversityEntry.COLUMN_UNIVERSITYID)));
                University.setName(res.getString(res.getColumnIndex(TableContract.UniversityEntry.COLUMN_NAME)));
                University.setAddress(res.getString(res.getColumnIndex(TableContract.UniversityEntry.COLUMN_ADDRESS)));
                res.moveToNext();
            }
            res.close();
        } catch (Exception e) {
            System.out.println("While searching university information found the following exception: " + e);
        }
    }
}
