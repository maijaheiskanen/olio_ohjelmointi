package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class activityAdmin_Meals extends AppCompatActivity {

    private String restaurantID;
    private int type;
    private Spinner typeSpinner, restaurantSpinner;
    private EditText nameInput;

    // Tried to create this in own class so that this method could be called from there but didn't succeed, so now I have this method in every class.
    private void loadActivity(Class where) {
        Intent intent = new Intent(this, where);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    // Bottom navigation view control.
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.admin_navigation_users:
                    loadActivity(activityAdmin_userInformation.class);
                    return true;
                case R.id.admin_navigation_restaurants:
                    loadActivity(activityAdmin_Restaurants.class);
                    return true;
                case R.id.admin_navigation_meals:
                    loadActivity(activityAdmin_Meals.class);
                    return true;
                case R.id.admin_navigation_reviews:
                    loadActivity(activityAdmin_Reviews.class);
                    return true;
                case R.id.admin_navigation_account:
                    loadActivity(activity_Account.class);
                    return true;
            }
            return false;
        }
    };

    private void initRecyclerView(ArrayList<Meal> mealList) {
        RecyclerView mealListing = findViewById(R.id.adminMealListing);
        RecyclerView.Adapter adapter = new AdminMealsAdapter(this, mealList);
        mealListing.setAdapter(adapter);
        mealListing.setLayoutManager(new LinearLayoutManager(this));
        mealListing.setHasFixedSize(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin__meals);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setSelectedItemId(R.id.admin_navigation_meals);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        nameInput = findViewById(R.id.mealNameInput);
        typeSpinner = createTypeSpinner(R.id.mealTypeSpinner);
        restaurantSpinner = createRestaurantSpinner(R.id.mealRestaurantSpinner, University.getRestaurantList());

        initRecyclerView(DatabaseSearcher.searchMealsAdmin(DatabaseInsert.getDatabase(), CurrentUser.getUserID()));
    }

    public ArrayList<String> createRestaurantStringList(ArrayList<Restaurant> list) {
        ArrayList<String> restaurantList = new ArrayList<>();

        University.updateAdminRestaurantList();
        for (Restaurant r : list) {
            if (University.getAdminRestaurantList().contains(r.getID())) {
                restaurantList.add(r.getID() + " " + r.getName());
            }
        }

        return restaurantList;
    }

    public ArrayList<String> createTypeStringList() {
        ArrayList<String> typeList = new ArrayList<>();

        typeList.add("Kotoisia makuja");
        typeList.add("Kokin suositus");
        typeList.add("Kasvisherkkuja");

        return typeList;
    }

    public Spinner createRestaurantSpinner(int id, ArrayList<Restaurant> restaurantList) {
        Spinner spinner = findViewById(id);
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                createRestaurantStringList(restaurantList)
        );
        spinner.setAdapter(adapteri);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                restaurantID = parent.getSelectedItem().toString().split(" ")[0];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                parent.setSelection(1);
            }
        });
        return spinner;
    }

    public Spinner createTypeSpinner(int id) {
        Spinner spinner = findViewById(id);
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                createTypeStringList()
        );
        spinner.setAdapter(adapteri);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String stringType = parent.getSelectedItem().toString();
                if (stringType.equals("Kotoisia makuja")) {
                    type = 1;
                } else if (stringType.equals("Kokin suositus")) {
                    type = 2;
                } else if (stringType.equals("Kasvisherkkuja")) {
                    type = 3;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                parent.setSelection(1);
            }
        });
        return spinner;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mealSaveButton:
                System.out.println("Button clicked");
                if (DatabaseInsert.insertMeal(DatabaseInsert.getDatabase(), Integer.parseInt(restaurantID), nameInput.getText().toString(), type, "0", "0")) {
                    Toast.makeText(this, "Meal saved.",
                            Toast.LENGTH_LONG).show();
                    University.getRestaurant(restaurantID).addMealList(DatabaseSearcher.searchLastMeal(DatabaseInsert.getDatabase()));
                    // Refresh the RecyclerView.
                    initRecyclerView(DatabaseSearcher.searchMealsAdmin(DatabaseInsert.getDatabase(), CurrentUser.getUserID()));

                } else {
                    Toast.makeText(this, "Couldn't save the restaurant.",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

}
