package com.example.harkkatyo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class AdminUsersReviewsAdapter extends RecyclerView.Adapter<AdminUsersReviewsAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<Review> mReviewList = new ArrayList<>();

    public AdminUsersReviewsAdapter(Context context, ArrayList<Review> reviewList) {
        mReviewList = reviewList;
        mContext = context;
    }

    @NonNull
    @Override
    public AdminUsersReviewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_admin_users_reviews_list_item, viewGroup, false); //TODO: Change right layout here when it is done.
        AdminUsersReviewsAdapter.ViewHolder holder = new AdminUsersReviewsAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AdminUsersReviewsAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.meal.setText(mReviewList.get(i).getMeal());
        viewHolder.grade.setText(mReviewList.get(i).getGrade());
        viewHolder.verbal.setText(mReviewList.get(i).getVerbal());
        viewHolder.user.setText(mReviewList.get(i).getUserName());
        viewHolder.date.setText(mReviewList.get(i).getDate());

    }

    @Override
    public int getItemCount() {
        return mReviewList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView meal, grade, verbal, user, date;
        private LinearLayout reviewListItemLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            meal = itemView.findViewById(R.id.mealName);
            grade = itemView.findViewById(R.id.grade);
            verbal = itemView.findViewById(R.id.verbal);
            user = itemView.findViewById(R.id.user);
            date = itemView.findViewById(R.id.dateOutput);
            reviewListItemLayout = itemView.findViewById(R.id.review_list_item_layout);
        }
    }

}
