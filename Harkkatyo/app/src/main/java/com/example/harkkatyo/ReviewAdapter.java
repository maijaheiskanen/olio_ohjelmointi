package com.example.harkkatyo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

// This class contains methods to set up the recyclerview for browsing the reviews.

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<Review> mReviewList = new ArrayList<>();

    public ReviewAdapter(Context context, ArrayList<Review> reviewList) {
        mReviewList = reviewList;
        mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_review_list_item, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        viewHolder.meal.setText(mReviewList.get(i).getMeal());
        viewHolder.grade.setText(mReviewList.get(i).getGrade());
        viewHolder.verbal.setText(mReviewList.get(i).getVerbal());
        viewHolder.user.setText(mReviewList.get(i).getUserName());
        viewHolder.date.setText(mReviewList.get(i).getDate());

        // Set the delete button enabled depending on whether the review is users own or not.
        if (mReviewList.get(i).getUserID().equals(CurrentUser.getUserID())) {
            System.out.println(mReviewList.get(i).getUserID() + ", " + CurrentUser.getUserID());
            viewHolder.deleteButton.setEnabled(true);

        } else {
            System.out.println("Reviews maker: " + mReviewList.get(i).getUserID() + ", Current user: " + CurrentUser.getUserID());
            viewHolder.deleteButton.setEnabled(false);
            viewHolder.deleteButton.setBackgroundColor(0x00fffff);
        }
        viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Setting the button not clickable after first click will prevent the app from crashing by trying to remove the same list item twice.
                viewHolder.deleteButton.setEnabled(false);

                // Saving the position of removable item.
                int position = viewHolder.getAdapterPosition();

                // Removing the item from database.
                DatabaseRemover.removeReview(Integer.parseInt(mReviewList.get(position).getReviewID()), mReviewList.get(position).getMealID());

                // Removing the item from the recyclerview.
                removeItem(position);

                Toast.makeText(mContext, "Review deleted!",
                        Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mReviewList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView meal, grade, verbal, user, date;
        private Button deleteButton;
        private LinearLayout reviewListItemLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            meal = itemView.findViewById(R.id.mealName);
            grade = itemView.findViewById(R.id.grade);
            verbal = itemView.findViewById(R.id.verbal);
            user = itemView.findViewById(R.id.user);
            date = itemView.findViewById(R.id.dateOutput);
            deleteButton = itemView.findViewById(R.id.deleteButton);
            reviewListItemLayout = itemView.findViewById(R.id.review_list_item_layout);
        }
    }

    private void removeItem(int position) {
        mReviewList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mReviewList.size());
    }
}
