package com.example.viikko11_3;

import android.app.Application;
import android.content.Context;

import com.example.viikko11_3.Helper.LocaleHelper;

public class MainApplication extends Application {
    ///////

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
    }

    //////
}
