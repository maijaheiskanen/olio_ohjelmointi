package com.example.harkkatyo;

// This class contains all the methods to update information in the database.

// I use try-catch with some SQLite things, but those wont have exceptions so the try-catch is kinda useless.
// (If I understood it right, at least it didn't print any exception when I tried to debug)

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.sql.PreparedStatement;
import java.sql.SQLSyntaxErrorException;
import java.sql.Statement;

public class DatabaseUpdater {

    private DatabaseUpdater() {}

    // This String statement and execSQL is very very very bad and questionable way of doing this, because now user can,
    // for example, write in reviews free feedback something bad things (' "; DROP TABLE ', ' ";# ', ' IdAdmin = 1 ', etc.) and they will be executed.
    // I would fix with db.update() but no time Xd.

    // Updates given user information.
    public static boolean updateUser(SQLiteDatabase db, User userInfo) {
        String statement;
        System.out.println("Olion tiedot: " + userInfo.getEmail() + " " + userInfo.getName());

        statement = "UPDATE " + TableContract.UserEntry.TABLE_NAME + " SET " +
                TableContract.UserEntry.COLUMN_NAME + " = \"" + userInfo.getName() + "\", " +
                TableContract.UserEntry.COLUMN_EMAIL + " = \"" + userInfo.getEmail() + "\", " +
                TableContract.UserEntry.COLUMN_PASSWORD + " = \"" + userInfo.getPassword() + "\"" +
                "WHERE " + TableContract.UserEntry.COLUMN_USERID + " = \"" + userInfo.getUserID() + "\" " +
                ";";



        try {
            db.execSQL(statement);
            System.out.println("User update was successful.");
            return true;
        } catch (SQLException e) { //TODO: FIND OUT WHAT EXCEPTIONS COULD OCCUR AND PUT THEM IN HERE!!!½!!!1!
            return false;
        }
    }

    // Updates given reviews information.
    public static boolean updateReview(SQLiteDatabase db, String reviewID, String mealID, int grade, String verbal) {
        String statement;

        statement = "UPDATE " + TableContract.ReviewEntry.TABLE_NAME + " SET " +
                TableContract.ReviewEntry.COLUMN_MEALID + " = \"" + mealID + "\", " +
                TableContract.ReviewEntry.COLUMN_GARDE + " = \"" + grade + "\", " +
                TableContract.ReviewEntry.COLUMN_VERBALEVALUATION + " = \"" + verbal + "\" "  +
                " WHERE " + TableContract.ReviewEntry.COLUMN_REVIEWID + " = \"" + reviewID + "\" " +
                ";";

                try {
                    db.execSQL(statement);
                    for (Restaurant r : University.getRestaurantList()) {
                        for (Meal m : r.getMealList()) {
                            if (m.getId().equals(mealID)) {
                                r.updateMealList();
                            }
                        }
                    }
                    return true;
                } catch (Exception e) {
                    return false;
                }
    }

    // Updates given reviews attribute Showable either "1" or "0" depending on given attribute.
    public static boolean updateReviewShowable(SQLiteDatabase db, boolean b, String reviewID) {
        String statement;
        String showable;

        if (b) {
            showable = "1";
        } else {
            showable = "0";
        }

        statement = "UPDATE " + TableContract.ReviewEntry.TABLE_NAME + " SET " +
                TableContract.ReviewEntry.COLUMN_SHOWABLE + " = \"" + showable + "\"" +
                " WHERE " + TableContract.ReviewEntry.COLUMN_REVIEWID + " = \"" + reviewID + "\"" +
                ";";

                try {
                    db.execSQL(statement);
                    for (Restaurant r : University.getRestaurantList()) {
                        for (Meal m : r.getMealList()) {
                            for (Review re : m.getReviewList()) {
                                if (re.getReviewID().equals(reviewID)) {
                                    m.updateReviewList();
                                }
                            }
                        }
                    }
                    return true;
                } catch (Exception e) {
                    return false;
                }
    }

    // Updates given restaurants attribute Available either "1" or "0" depending on given attribute.
    public static boolean updateRestaurantAvailable(SQLiteDatabase db, boolean b, String restaurantID) {
        String statement;
        String available;

        if (b) {
            available = "1";
        } else {
            available = "0";
        }

        statement = "UPDATE " + TableContract.RestaurantEntry.TABLE_NAME + " SET " +
                TableContract.RestaurantEntry.COLUMN_AVAILABLE + " = \"" + available + "\"" +
                " WHERE " + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + " = \"" + restaurantID + "\"" +
                ";";

        try {
            db.execSQL(statement);
            University.updateRestaurantList();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    // For updating restaurant in database (admin view)
    public static boolean updateRestaurant(SQLiteDatabase db, String restaurantID, String restaurantName, String restaurantAddress) {
        String statement;

        statement = "UPDATE " + TableContract.RestaurantEntry.TABLE_NAME + " SET " +
                TableContract.RestaurantEntry.COLUMN_NAME + " = \"" + restaurantName + "\", " +
                TableContract.RestaurantEntry.COLUMN_ADDRESS + " = \"" + restaurantAddress+ "\"" +
                " WHERE " + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + " = \"" + restaurantID + "\"" +
                ";";

        try {
            db.execSQL(statement);
            University.updateRestaurantList();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


}
