package com.example.harkkatyo;


import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText emailInput, passwordInput;
    private String email, password;

    // Tried to create this in own class so that this method could be called from there but didn't succeed, so now I have this method in every class.
    private void loadActivity(Class where) {
        Intent intent = new Intent(this, where);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Opening the database and saving it in DatabaseInsert class so it can be used all over the application.
        DatabaseInsert.setDatabase(new DatabaseHelper(this).getWritableDatabase());

        // Creating some test data if database doesn't have university (universities can't be added or deleted).
        if (!CreateTestData.doesUniversityExist(DatabaseInsert.getDatabase())) {
            CreateTestData.createTestData();
        }

        emailInput = findViewById(R.id.emailInput);
        passwordInput = findViewById(R.id.passwordInput);

        // Loads information from database and saves it in objects.
        DatabaseLoader.loadFromDatabase();
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginButton:
                //TODO: Check from the database if email and password were right.
                //Resource: https://www.tutorialspoint.com/how-to-use-select-query-in-android-sqlite

                password = DatabaseSearcher.searchPasswordByEmail(DatabaseInsert.getDatabase(), emailInput.getText().toString());
                if (emailInput.getText().toString().length() == 0) {
                    System.out.println("Email field was empty.");
                    Toast.makeText(this, "Write both email and password to log in.",
                            Toast.LENGTH_LONG).show();
                } else if (password == null) {
                    System.out.println("No such email as given.");
                    Toast.makeText(this, "Wrong email or password. (password null)",
                            Toast.LENGTH_LONG).show();
                } else if (password.equals(passwordInput.getText().toString())) {
                    System.out.println("Logging in was successful. Moving to menus view in app.");
                    Toast.makeText(this, "Logged in successfully.",
                            Toast.LENGTH_LONG).show();

                    CurrentUser.setUserID(DatabaseSearcher.searchUserID(DatabaseInsert.getDatabase(), emailInput.getText().toString(), passwordInput.getText().toString()));
                    User userInfo = DatabaseSearcher.searchUserInfo(DatabaseInsert.getDatabase(), CurrentUser.getUserID());
                    CurrentUser.setEmail(userInfo.getEmail());
                    CurrentUser.setName(userInfo.getName());
                    CurrentUser.setIsAdmin(DatabaseSearcher.searchIsAdmin(DatabaseInsert.getDatabase(), CurrentUser.getUserID()));

                    if (CurrentUser.getIsAdmin()) {
                        loadActivity(activityAdmin_userInformation.class);
                    } else {
                        loadActivity(activity_Menus.class);
                    }
                } else {
                    System.out.println("Logging in wasn't successful.");
                    Toast.makeText(this, "Wrong email or password.",
                            Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.noAccountTextButton:
                loadActivity(activity_CreateAccount.class);
                break;
        }

    }
}
