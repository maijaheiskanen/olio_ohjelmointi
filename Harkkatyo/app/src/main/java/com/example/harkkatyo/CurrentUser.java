package com.example.harkkatyo;

// Contains the information of current user.

public class CurrentUser {
    private static String userID = null, name = null, email = null;
    private static boolean isAdmin = false;

    private CurrentUser() {}

    public static void setUserID(String id) {
        userID = id;
    }
    public static void setName(String name) {
        CurrentUser.name = name;
    }
    public static void setEmail(String email) {
        CurrentUser.email = email;
    }

    public static void setIsAdmin(boolean isAdmin) {
        CurrentUser.isAdmin = isAdmin;
    }

    public static String getUserID() {
        return userID;
    }
    public static String getEmail() {
        return email;
    }
    public static String getName() {
        return name;
    }
    public static boolean getIsAdmin() {
        return isAdmin;
    }
}
