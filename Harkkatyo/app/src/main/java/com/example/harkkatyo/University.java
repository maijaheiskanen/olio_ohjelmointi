package com.example.harkkatyo;

// Keeps track on university restaurants.

import java.util.ArrayList;

public class University {
    private static String name, address, universityID;
    private static ArrayList<Restaurant> restaurantList = new ArrayList<>();
    private static ArrayList<String> adminRestaurantList = new ArrayList<>();
    private static University university = new University();

    // In this case, there can be only one university.
    private University() {
        DatabaseSearcher.searchAndSetUniversity(DatabaseInsert.getDatabase());
    }

    public static University getUniversity() {
        return university;
    }

    public static void updateRestaurantList() {
        restaurantList = DatabaseSearcher.searchRestaurants(DatabaseInsert.getDatabase(), universityID, name);
    }

    public static ArrayList<Restaurant> getRestaurantList() {
        updateRestaurantList();
        return restaurantList;
    }

    public static ArrayList<Restaurant> getAvailableRestaurantList() {
        updateRestaurantList();
        ArrayList<Restaurant> availableRestaurants = new ArrayList<>();
        for (Restaurant r : restaurantList) {
            System.out.println("Restaurant :" + r.getName() + " Available: " + r.isAvailable());
            if (r.isAvailable()) {
                availableRestaurants.add(r);
            }
        }
        return availableRestaurants;
    }

    public static void updateAdminRestaurantList() {
        adminRestaurantList = DatabaseSearcher.searchAdminsRestaurants(DatabaseInsert.getDatabase(), CurrentUser.getUserID());
    }

    public static void addRestaurantList(Restaurant r) {
        restaurantList.add(r);
    }

    public static void removeRestaurantList(Restaurant r) {
        restaurantList.remove(r);
    }

    public static ArrayList<String> getAdminRestaurantList() {
        return adminRestaurantList;
    }

    public static void setAddress(String pAddress) {
        address = pAddress;
    }
    public static void setName(String pName) {
        name = pName;
    }
    public static void setUniversityID(String pUniversityID) {
        universityID = pUniversityID;
    }

    public static String getUniversityID() {
        return universityID;
    }
    public static String getName() {
        return name;
    }
    public static String getAddress() {
        return address;
    }

    public static Restaurant getRestaurant(String restaurantID) {
        for (Restaurant r : restaurantList) {
            if (r.getID().equals(restaurantID)) {
                System.out.println("University found restaurant: " + r.getName());
                return r;
            }
        }
        System.out.println("University didn't contain a restaurant with given ID.");
        return null;
    }
}
