package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class activityAdmin_Restaurants extends AppCompatActivity {

    private EditText restaurantName, restaurantAddress;
    private Spinner restaurantSpinner;
    private Restaurant restaurant;

    // Tried to create this in own class so that this method could be called from there but didn't succeed, so now I have this method in every class.
    private void loadActivity(Class where) {
        Intent intent = new Intent(this, where);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    // Bottom navigation view control.
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.admin_navigation_users:
                    loadActivity(activityAdmin_userInformation.class);
                    return true;
                case R.id.admin_navigation_restaurants:
                    loadActivity(activityAdmin_Restaurants.class);
                    return true;
                case R.id.admin_navigation_meals:
                    loadActivity(activityAdmin_Meals.class);
                    return true;
                case R.id.admin_navigation_reviews:
                    loadActivity(activityAdmin_Reviews.class);
                    return true;
                case R.id.admin_navigation_account:
                    loadActivity(activity_Account.class);
                    return true;
            }
            return false;
        }
    };

    private void initRecyclerView(ArrayList<Restaurant> restaurantList) {
        RecyclerView reviewListing = findViewById(R.id.adminRestaurantsListing);
        RecyclerView.Adapter adapter = new AdminRestaurantsAdapter(this, restaurantList);
        reviewListing.setAdapter(adapter);
        reviewListing.setLayoutManager(new LinearLayoutManager(this));
        reviewListing.setHasFixedSize(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("In admins restaurants view");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin__restaurants);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setSelectedItemId(R.id.admin_navigation_restaurants);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        restaurantName = findViewById(R.id.restaurantNameInput);
        restaurantAddress = findViewById(R.id.restaurantAddressInput);

        University.updateAdminRestaurantList();
        restaurantSpinner = createRestaurantSpinner(R.id.adminRestaurantSpinner, University.getRestaurantList());
        initRecyclerView(University.getRestaurantList());
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.restaurantSaveButton:

                System.out.println("Button clicked");
                // If there was no restaurant selected from the spinner, saves a new restaurant in the database.
                if (restaurantSpinner.getSelectedItem().equals("Edit restaurant:")) {
                    if (DatabaseInsert.insertRestaurant(DatabaseInsert.getDatabase(), Integer.parseInt(University.getUniversityID()), restaurantName.getText().toString(), restaurantAddress.getText().toString())) {
                        if (DatabaseInsert.insertMaintenance(DatabaseInsert.getDatabase(), CurrentUser.getUserID(), Integer.parseInt(DatabaseSearcher.searchLastRestaurantID(DatabaseInsert.getDatabase())))) {
                            Toast.makeText(this, "Restaurant saved.",
                                    Toast.LENGTH_LONG).show();
                            // Add in university.
                            University.addRestaurantList(DatabaseSearcher.searchLastRestaurant(DatabaseInsert.getDatabase(), University.getUniversityID(), University.getName()));
                            // Refresh the RecyclerView and spinner.
                            initRecyclerView(University.getRestaurantList());
                            restaurantSpinner = createRestaurantSpinner(R.id.adminRestaurantSpinner, University.getRestaurantList());
                        } else {
                            Toast.makeText(this, "Couldn't save the maintenance.",
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(this, "Couldn't save the restaurant.",
                                Toast.LENGTH_LONG).show();

                        break;
                    }
                    // If a restaurant was selected from the spinner, updates that restaurants information in the database.
                } else {
                    System.out.println("Got in the updater section.");
                    if (DatabaseUpdater.updateRestaurant(DatabaseInsert.getDatabase(), restaurantSpinner.getSelectedItem().toString().split(" ")[0], restaurantName.getText().toString(), restaurantAddress.getText().toString())) {
                        Toast.makeText(this, "Restaurant updated.",
                                Toast.LENGTH_LONG).show();
                        // Update in university object.
                        University.getRestaurant(restaurantSpinner.getSelectedItem().toString().split(" ")[0]).updateInfo(restaurantName.getText().toString(), restaurantAddress.getText().toString());
                        // Refresh the RecyclerView adn spinner.
                        initRecyclerView(University.getRestaurantList());
                        restaurantSpinner = createRestaurantSpinner(R.id.adminRestaurantSpinner, University.getRestaurantList());
                    }
                }
                break;
        }

    }

    public ArrayList<String> createRestaurantStringList(ArrayList<Restaurant> restaurantList) {
        ArrayList<String> restaurantStringList = new ArrayList<>();
        restaurantStringList.add("Edit restaurant:");

        for (Restaurant r : restaurantList) {
            if (University.getAdminRestaurantList().contains(r.getID())) {
                restaurantStringList.add(r.getID() + " " + r.getName());
            }
        }

        return restaurantStringList;
    }

    public void clearFields() {
        restaurantName.setText("");
        restaurantAddress.setText("");
    }

    public void fillFields(Restaurant r) {
        restaurantName.setText(r.getName());
        restaurantAddress.setText(r.getAddress());
    }

    public Spinner createRestaurantSpinner(int id, ArrayList<Restaurant> restaurantList) {
        Spinner spinner = findViewById(id);
        ArrayList<String> reviewStringList = createRestaurantStringList(restaurantList);
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                reviewStringList
        );
        spinner.setAdapter(adapteri);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getSelectedItem().equals("Edit restaurant:")) {
                    clearFields();
                    restaurant = null;
                } else {
                    String[] list = parent.getSelectedItem().toString().split(" ");
                    // Searches restaurant by id from spinner.
                    restaurant = DatabaseSearcher.searchRestaurantByID(DatabaseInsert.getDatabase(), list[0]);
                    fillFields(restaurant);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                clearFields();
                restaurant = null;

            }
        });
        return spinner;
    }
}
