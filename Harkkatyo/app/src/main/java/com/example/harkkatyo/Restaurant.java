package com.example.harkkatyo;

// Contains basic information and menus and meal-objects.

import java.util.ArrayList;

public class Restaurant {
    private String ID, name, address, universityName = null, universityID = null;
    private boolean available;
    private ArrayList<Meal> mealList = new ArrayList<>();
    private ArrayList<Menu> menuList = new ArrayList<>();

    public Restaurant(String pID, String pName, String pAddress, String pUniversityName, String pUniversityID, boolean pAvailable) {
        ID = pID;
        name = pName;
        address = pAddress;
        universityID = pUniversityID;
        universityName = pUniversityName;
        available = pAvailable;
    }

    public Restaurant(String pID, String pName, String pAddress, boolean pAvailable) {
        ID = pID;
        name = pName;
        address = pAddress;
        available = pAvailable;
    }

    public ArrayList<Meal> getMealList() {
        return mealList;
    }

    public ArrayList<Menu> getMenuList() {
        return menuList;
    }

    public void updateMealList() {
        ArrayList<Meal> dMealList = DatabaseSearcher.searchMeals(DatabaseInsert.getDatabase());
        mealList = new ArrayList<>();
        for (Meal m : dMealList) {
            if (m.getRestaurantID().equals(ID)) {
                mealList.add(m);
            }
        }
    }

    public void updateMenulist() {
        menuList = DatabaseSearcher.searchMenus(DatabaseInsert.getDatabase());
        ArrayList<Menu> removableMenus = new ArrayList<>();
        for (Menu m : menuList) {
            if (!m.getRestaurantID().equals(ID)) {
                System.out.println("IDs didn't match: " + m.getRestaurantID() + " " + ID);
                removableMenus.add(m);
            }
        }

        for (Menu m : removableMenus) {
            menuList.remove(m);
            System.out.println("RestaurantID: " + m.getRestaurantID() + " | MenuID: " + m.getMenuID() + " | Date: " + m.getDate() + " | removed.");
        }
    }

    public void addMealList(Meal m) {
        mealList.add(m);
    }

    public void addMenuList(Menu m) {
        menuList.add(m);
    }

    public void removeMealList(Meal m) {
        mealList.remove(m);
    }

    public void removeMenuList(Menu m) {
        menuList.remove(m);
    }

    public String getID() {
        return ID;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    // There is only one University so this is kinda useless.
    public String getUniversityID() {
        return universityID;
    }

    public String getUniversityName() {
        return universityName;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailableStatus(boolean b) {
        available = b;
    }

    public void updateInfo(String newName, String newAddress) {
        name = newName;
        address = newAddress;
    }
}
