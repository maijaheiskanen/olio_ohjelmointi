package com.example.harkkatyo;

// This class contains the methods to remove data from the database.

// I use try-catch with some SQLite things, but those wont have exceptions so the try-catch is kinda useless.
// (If I understood it right, at least it didn't print any exception when I tried to debug)

public class DatabaseRemover {

    public static boolean removeReview(int removedID, String mealID) {
        try {
            if (DatabaseInsert.getDatabase().delete(TableContract.ReviewEntry.TABLE_NAME, TableContract.ReviewEntry.COLUMN_REVIEWID + " = \"" + removedID + "\"", null) > 0) {
                for (Restaurant r : University.getRestaurantList()) {
                    for (Meal m : r.getMealList()) {
                        if (m.getId().equals(mealID)) {
                            m.updateReviewList();
                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception e) { //TODO: real exceptions and blah blah
            return false;
        }
    }

    public static boolean removeRestaurant(int removedID) {
        try {
            if (DatabaseInsert.getDatabase().delete(TableContract.RestaurantEntry.TABLE_NAME, TableContract.RestaurantEntry.COLUMN_RESTAURANTID + " = \"" + removedID + "\"", null) > 0) {
                University.updateRestaurantList();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) { //TODO: real exceptions and blah blah
            return false;
        }
    }

    public static boolean removeMeal(int removedID) {
        try {
            if (DatabaseInsert.getDatabase().delete(TableContract.MealEntry.TABLE_NAME, TableContract.MealEntry.COLUMN_MEALID + " = \"" + removedID + "\"", null) > 0) {
                for (Restaurant r : University.getRestaurantList()) {
                    for (Meal m : r.getMealList()) {
                        if (m.getId().equals(removedID)) {
                            r.updateMealList();
                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception e) { //TODO: real exceptions and blah blah
            return false;
        }
    }

    public static  boolean removeMenu(int removedID) {
        try {
            if (DatabaseInsert.getDatabase().delete(TableContract.MenuEntry.TABLE_NAME, TableContract.MenuEntry.COLUMN_MENUID+ " = \"" + removedID + "\"", null) > 0) {
                for (Restaurant r : University.getRestaurantList()) {
                    for (Menu m : r.getMenuList()) {
                        if (m.getMenuID().equals(removedID)) {
                            r.updateMenulist();
                        }
                    }
                }
                return true;
            } else {
                return false;
            }
        } catch (Exception e) { //TODO: real exceptions and blah blah
            return false;
        }
    }
}
