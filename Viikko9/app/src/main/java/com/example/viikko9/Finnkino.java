package com.example.viikko9;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class Finnkino {
    private ArrayList<Teatteri> teatteriLista = new ArrayList<>();
    private ArrayList<Elokuva> elokuvaLista = new ArrayList<>();
    private ArrayList<Paivamaara> pvmLista = new ArrayList<>();
    private static Finnkino fk = new Finnkino();

    private Finnkino() {

    }

    public static Finnkino getInstance() {
        return fk;
    }

    public void lisaaTeatteri(Teatteri t) {
        teatteriLista.add(t);
    }
    public void lisaaElokuva(Elokuva t) {
        elokuvaLista.add(t);
    }
    public void lisaaPvm(Paivamaara t) {
        pvmLista.add(t);
    }


    public ArrayList<String> getTeatteriNimilista() {
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Kaikki");
        for (Teatteri t : teatteriLista) {
            lista.add(t.getNimi());
        }
        return lista;
    }
    public ArrayList<String> getElokuvaNimilista() {
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Kaikki");
        for (Elokuva t : elokuvaLista) {
            lista.add(t.getNimi());
        }
        return lista;
    }
    public ArrayList<String> getPvmLista() {
        ArrayList<String> lista = new ArrayList<>();
        for (Paivamaara t : pvmLista) {
            lista.add(t.getPvm());
        }
        return lista;
    }



    // Listat voidaan nollata niiden paivittamista varten.
    public void nollaaTeatterilista() {
        teatteriLista = new ArrayList<>();
    }
    public void nollaaElokuvalista() {
        elokuvaLista = new ArrayList<>();
    }
    public void nollaaPvmlista() {
        pvmLista = new ArrayList<>();
    }

    public String findID(String nimi) {
        for (Teatteri t :teatteriLista) {
            if (t.getNimi().equals(nimi)) {
                return t.getID();
            }
        }
        return null;
    }

    public void teatteritReadXML() {
        String urlString;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            urlString = "https://www.finnkino.fi/xml/TheatreAreas/";
            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getDocumentElement().getElementsByTagName("TheatreArea");
            this.nollaaTeatterilista();

            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    Teatteri teatteri = new Teatteri(element.getElementsByTagName("ID").item(0).getTextContent(), element.getElementsByTagName("Name").item(0).getTextContent());
                    if (teatteri.getNimi().equals("Valitse alue/teatteri")) {
                        teatteri = new Teatteri(element.getElementsByTagName("ID").item(0).getTextContent(), "Kaikki");
                        continue;
                    /* Jos halutaan rajata alueet pois valinnoista
                    } else if (!teatteri.getNimi().contains(":")) {
                        //System.out.println(":\nAAAAAAAAAA\nAAAAAAAAAAAAAAA\nAAAAAAAAAAAAAAAAA\nAAAAAAA\nAAAAAAAAAAAAAAAAA\nAAAAA");
                        continue;
                        */
                    }
                    this.lisaaTeatteri(teatteri);
                }
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //TODO: Lisaa naita kunnes niita on tarpeeksi. AAA Valintarakenne, jolla tarkistetaan mita tietoja on annettu ja sen perusteella suoritetaan tarvittavat haut.

    public String elokuvatReadXML(String teatteri, String nimi, String pvm, String aloitusAika, String lopetusAika) {
        String urlString;

        String s = "Xd jotain meni pieleen";

        if (teatteri == "Kaikki") {
            s = "Löydetyt elokuvat:\n";
            for (Teatteri t : teatteriLista) {
                if (!t.getNimi().contains(":") || t.getNimi().equals("Kaikki")) {
                    continue;
                }
                try {
                    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    urlString = "http://www.finnkino.fi/xml/Schedule/?area=" + t.getID() + "&dt=" + pvm;
                    Document doc = builder.parse(urlString);
                    doc.getDocumentElement().normalize();
                    System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

                    NodeList nList = doc.getDocumentElement().getElementsByTagName("Show");

                    SimpleDateFormat muokkaaAjaksi = new SimpleDateFormat("hh:mm:ss");
                    Date aika;
                    Date alku;
                    Date loppu;

                    for (int i = 0; i < nList.getLength(); i++) {
                        Node node = nList.item(i);

                        if (node.getNodeType() == Node.ELEMENT_NODE) {
                            Element element = (Element) node;

                            //System.out.println(element.getElementsByTagName("Title").item(0).getTextContent());
                            //System.out.println(nimi);

                            try {
                                if (aloitusAika.equals("")) {
                                    aloitusAika = "00:00:00";
                                    System.out.println("Nollattiin aloitusaika.");
                                }
                                if (lopetusAika.equals("")) {
                                    lopetusAika = "23:59:59";
                                    System.out.println("Nollattiin lopetusaika.");
                                }

                                // Otetaan pvm date-juttuun.
                                aika = muokkaaAjaksi.parse((element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T"))[1]);
                                alku = muokkaaAjaksi.parse(aloitusAika);
                                loppu = muokkaaAjaksi.parse(lopetusAika);

                                System.out.println("Muokattiin ajat niihin formaatteihin.");
                            } catch (Exception e) {
                                System.out.println("Aikaa ei pystynyt laittamaan silleen jotenkin.");
                                alku = muokkaaAjaksi.parse("00:00:00");
                                loppu = muokkaaAjaksi.parse("23:59:99");
                                aika = muokkaaAjaksi.parse("12:00:00");
                                return "Aika annettiin väärässä formaatissa. Yritä uudelleen.";
                            }

                            if ( aika.after(alku) && aika.before(loppu) && (nimi.equals("Kaikki") || (element.getElementsByTagName("Title").item(0).getTextContent().equals(nimi)))) {
                                s = s + element.getElementsByTagName("Title").item(0).getTextContent() + " " + element.getElementsByTagName("Theatre").item(0).getTextContent() + "\n    Ajankohta: " + element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T")[0] + " " + element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T")[1] + "\n";
                            }
                        }
                    }
                } catch (SAXException e) {
                    return "Jotain tapahtui minkä ei ois pitäny tapahtuu: " + e.toString();
                } catch (IOException e) {
                    return "Jotain tapahtui minkä ei ois pitäny tapahtuu: " + e.toString();
                } catch (ParseException e) {
                    return "Jotain tapahtui minkä ei ois pitäny tapahtuu: " + e.toString();
                } catch (Exception e) {
                    return "Jotain tapahtui minkä ei ois pitäny tapahtuu: " + e.toString();
                }
            }
        } else {
            try {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                urlString = "http://www.finnkino.fi/xml/Schedule/?area=" + findID(teatteri) + "&dt=" + pvm;
                Document doc = builder.parse(urlString);
                doc.getDocumentElement().normalize();
                System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

                NodeList nList = doc.getDocumentElement().getElementsByTagName("Show");
                s = "Löydetyt elokuvat:\n";

                SimpleDateFormat muokkaaAjaksi = new SimpleDateFormat("hh:mm:ss");
                Date aika;
                Date alku;
                Date loppu;

                for (int i = 0; i < nList.getLength(); i++) {
                    Node node = nList.item(i);

                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element = (Element) node;

                        //System.out.println(element.getElementsByTagName("Title").item(0).getTextContent());
                        //System.out.println(nimi);

                        try {
                            if (aloitusAika.equals("")) {
                                aloitusAika = "00:00:00";
                                System.out.println("Nollattiin aloitusaika.");
                            }
                            if (lopetusAika.equals("")) {
                                lopetusAika = "23:59:59";
                                System.out.println("Nollattiin lopetusaika.");
                            }

                            // Otetaan pvm date-juttuun.
                            aika = muokkaaAjaksi.parse((element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T"))[1]);
                            alku = muokkaaAjaksi.parse(aloitusAika);
                            loppu = muokkaaAjaksi.parse(lopetusAika);

                            System.out.println("Muokattiin ajat niihin formaatteihin.");
                        } catch (Exception e) {
                            System.out.println("Aikaa ei pystynyt laittamaan silleen jotenkin.");
                            alku = muokkaaAjaksi.parse("00:00:00");
                            loppu = muokkaaAjaksi.parse("23:59:99");
                            aika = muokkaaAjaksi.parse("12:00:00");
                            return "Aika annettiin väärässä formaatissa. Yritä uudelleen.";
                        }

                        if ( aika.after(alku) && aika.before(loppu) && (nimi.equals("Kaikki") || (element.getElementsByTagName("Title").item(0).getTextContent().equals(nimi)))) {
                            s = s + element.getElementsByTagName("Title").item(0).getTextContent() + " " + element.getElementsByTagName("Theatre").item(0).getTextContent() + "\n    Ajankohta: " + element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T")[0] + " " + element.getElementsByTagName("dttmShowStart").item(0).getTextContent().split("T")[1] + "\n";
                        }
                    }
                }
            } catch (SAXException e) {
                return s;
            } catch (IOException e) {
                return s;
            } catch (ParseException e) {
                return s;
            } catch (Exception e) {
                System.out.println("Jotain tapahtui minkä ei ois pitäny tapahtuu: ");
            }
        }
        return s;
    }

    public void elokuvatReadXML() {
        String urlString;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            urlString = "https://www.finnkino.fi/xml/Events/";
            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getDocumentElement().getElementsByTagName("Event");
            this.nollaaElokuvalista();

            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    Elokuva elokuva = new Elokuva(element.getElementsByTagName("Title").item(0).getTextContent());
                    this.lisaaElokuva(elokuva);
                }
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //TODO: Lisaa naita kunnes niita on tarpeeksi. AAA Valintarakenne, jolla tarkistetaan mita tietoja on annettu ja sen perusteella suoritetaan tarvittavat haut.





    public void pvmReadXML() {
        //System.out.println("pvmReadXML jutussa");
        String urlString;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            urlString = "https://www.finnkino.fi/xml/ScheduleDates/";
            Document doc = builder.parse(urlString);
            doc.getDocumentElement().normalize();
            System.out.println("Root element: " + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getDocumentElement().getElementsByTagName("dateTime");
            this.nollaaPvmlista();

            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                //System.out.println("AAAAAA" + node);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    //System.out.println("BBBBBB" + element);

                    Paivamaara paivamaara = new Paivamaara(element.getTextContent());
                    this.lisaaPvm(paivamaara);
                }
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
