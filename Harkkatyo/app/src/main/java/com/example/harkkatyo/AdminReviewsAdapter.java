package com.example.harkkatyo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AdminReviewsAdapter extends RecyclerView.Adapter<AdminReviewsAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<Review> mReviewList = new ArrayList<>();

    public AdminReviewsAdapter(Context context, ArrayList<Review> reviewList) {
        mReviewList = reviewList;
        mContext = context;
    }

    @NonNull
    @Override
    public AdminReviewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_admin_reviews_list_item, viewGroup, false);
        AdminReviewsAdapter.ViewHolder holder = new AdminReviewsAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AdminReviewsAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.meal.setText(mReviewList.get(i).getMeal());
        viewHolder.grade.setText(mReviewList.get(i).getGrade());
        viewHolder.verbal.setText(mReviewList.get(i).getVerbal());
        viewHolder.user.setText(mReviewList.get(i).getUserName());
        viewHolder.date.setText(mReviewList.get(i).getDate());
        // Sets the switch either true or false depending on reviews status.
        viewHolder.showableSwitch.setChecked(mReviewList.get(i).isShowable());

        viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Setting the button not clickable after first click will prevent the app from crashing by trying to remove the same list item twice.
                viewHolder.deleteButton.setEnabled(false);

                // Saving the position of removable item.
                int position = viewHolder.getAdapterPosition();

                // Removing the item from database.
                DatabaseRemover.removeReview(Integer.parseInt(mReviewList.get(position).getReviewID()), mReviewList.get(position).getMealID());

                // Remove from meal object.
                for (Restaurant r : University.getRestaurantList()) {
                    for (Meal m : r.getMealList()) {
                        for (Review re : m.getReviewList()) {
                            if (re.getReviewID().equals(mReviewList.get(position).getReviewID())) {
                                m.removeReviewList(re);
                            }
                        }
                    }
                }

                // Removing the item from the recyclerview.
                removeItem(position);

                Toast.makeText(mContext, "Review deleted!",
                        Toast.LENGTH_LONG).show();
            }
        });

        viewHolder.showableSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = viewHolder.getAdapterPosition();
                boolean success = DatabaseUpdater.updateReviewShowable(DatabaseInsert.getDatabase(), viewHolder.showableSwitch.isChecked(), mReviewList.get(position).getReviewID());
                 if (success) {
                     System.out.println("Reviews status changed.");
                     if (!viewHolder.showableSwitch.isChecked()) {
                         for (Restaurant r : University.getRestaurantList()) {
                             for (Meal m : r.getMealList()) {
                                 for (Review re : m.getReviewList()) {
                                     if (re.getReviewID().equals(mReviewList.get(position).getReviewID())) {
                                         re.setShowableStatus(false);
                                     }
                                 }
                             }
                         }
                         Toast.makeText(mContext, "Review hidden.",
                                 Toast.LENGTH_LONG).show();
                     } else {
                         for (Restaurant r : University.getRestaurantList()) {
                             for (Meal m : r.getMealList()) {
                                 for (Review re : m.getReviewList()) {
                                     if (re.getReviewID().equals(mReviewList.get(position).getReviewID())) {
                                         re.setShowableStatus(true);
                                     }
                                 }
                             }
                         }
                         Toast.makeText(mContext, "Review unhidden.",
                                 Toast.LENGTH_LONG).show();
                     }
                 } else {
                     System.out.println("Reviews status wasn't chanced successfully.");
                 }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mReviewList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView meal, grade, verbal, user, date;
        private LinearLayout reviewListItemLayout;
        private Switch showableSwitch;
        private Button deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            meal = itemView.findViewById(R.id.mealName);
            grade = itemView.findViewById(R.id.grade);
            verbal = itemView.findViewById(R.id.verbal);
            user = itemView.findViewById(R.id.user);
            date = itemView.findViewById(R.id.dateOutput);
            showableSwitch = itemView.findViewById(R.id.showableSwitch);
            deleteButton = itemView.findViewById(R.id.deleteButton);
            reviewListItemLayout = itemView.findViewById(R.id.review_list_item_layout);
        }
    }

    private void removeItem(int position) {
        mReviewList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mReviewList.size());
    }

}