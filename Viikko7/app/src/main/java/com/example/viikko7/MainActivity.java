package com.example.viikko7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;




public class MainActivity extends AppCompatActivity {

    TextView text;
    EditText newText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.teksti);
        newText = (EditText) findViewById(R.id.syote);
        text.setText("");

        newText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                tulostaTeksti(text);
                return true;
            }
        });

    }

    public void tulostaTeksti(View v) {
        System.out.println("Hello World!");

        String juttu = newText.getText().toString();
        text.setText(juttu);
    }

    public void nollaaTeksti(View v) {
        text.setText("Hello World!");
    }

}




