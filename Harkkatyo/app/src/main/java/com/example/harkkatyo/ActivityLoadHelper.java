package com.example.harkkatyo;

// This class is unused, because I didn't get it to work right.
// The basic idea was to have method that loads a new activity in one class,
// so I wouldn't have to write that method in every class again.

// I tried to google the answer but didn't find anything like this so gave up.

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

public class ActivityLoadHelper extends AppCompatActivity {
    private static ActivityLoadHelper activityLoadHelper = new ActivityLoadHelper();

    private ActivityLoadHelper() {
    }

    public static ActivityLoadHelper getActivityLoadHelper() {
        return activityLoadHelper;
    }

    public void loadActivity(Context context, Class where) {
        Intent intent = new Intent(context, where);
        startActivity(intent);
    }
}
