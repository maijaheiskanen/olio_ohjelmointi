package com.example.viikko11_3;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.example.viikko11_3.Helper.LocaleHelper;

import java.util.ArrayList;

import io.paperdb.Paper;

//TODO: TESTAAS TÄÄ SAATANA AI JUMALAUTA KU OTTAA PÄÄÄHÄN VITTU https://blog.guillaumeagis.eu/change-language/

public class Asetukset extends AppCompatActivity {

    TextView fonttikokoOtsikko, leveysOtsikko, korkeusOtsikko, rivimaaraOtsikko, muokkausKytkin, valitseKieliOtsikko;

    EditText fonttikokoKentta, leveysKentta, korkeusKentta, rivimaaraKentta, displayTextSyote;
    Switch muokkaus;
    Spinner kieliValitsin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("Päästiin asetuksiin asti.");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asetukset);

        fonttikokoKentta = findViewById(R.id.fonttikokoKentta);
        leveysKentta = findViewById(R.id.leveysKentta);
        korkeusKentta = findViewById(R.id.korkeusKentta);
        rivimaaraKentta = findViewById(R.id.rivimaaraKentta);

        System.out.println("Päästiin asetuksiin asti.2");

        AsetuksetOlio olio = AsetuksetOlio.getInstance();

        System.out.println("Päästiin asetuksiin asti.3");

        fonttikokoKentta.setText(Integer.toString(olio.getFonttikoko()));
        System.out.println("AAAAAA3");

        leveysKentta.setText(Integer.toString(olio.getLeveys()));
        korkeusKentta.setText(Integer.toString(olio.getKorkeus()));
        rivimaaraKentta.setText(Integer.toString(olio.getRivimaara()));

        System.out.println("Päästiin asetuksiin asti.4");

        muokkaus = findViewById(R.id.muokkausKytkin);
        muokkaus.setChecked(olio.getMuokkaus());

        displayTextSyote = findViewById(R.id.displayTextSyote);

        kieliValitsin = luoSpinneri(R.id.kieliSpinneri);

    }

    private void siirryPaanakymaan() {
        Intent intent = new Intent(Asetukset.this, MainActivity.class);
        startActivity(intent);
    }


    ///// jtn shaissee

    private void updateView(String lang) {
        Context context = LocaleHelper.setLocale(this, lang);
        Resources resources = context.getResources();

        fonttikokoOtsikko = findViewById(R.id.fonttikokoOtsikko);
        leveysOtsikko = findViewById(R.id.leveysOtsikko);
        korkeusOtsikko = findViewById(R.id.korkeusOtsikko);
        rivimaaraOtsikko = findViewById(R.id.rivimaaraOtsikko);
        muokkausKytkin = findViewById(R.id.muokkausKytkin);
        valitseKieliOtsikko = findViewById(R.id.valitseKieliOtsikko);

        fonttikokoOtsikko.setText(resources.getString(R.string.fonttikoko));
        leveysOtsikko.setText(resources.getString(R.string.leveys));
        korkeusOtsikko.setText(resources.getString(R.string.korkeus));
        rivimaaraOtsikko.setText(resources.getString(R.string.rivimaara));
        muokkausKytkin.setText(resources.getString(R.string.lukitse_tekstikentta));
        valitseKieliOtsikko.setText(resources.getString(R.string.valitse_kieli));


    }

    /////


    private Spinner luoSpinneri(int id) { //TODO: Teeppäs simmottiineen suatanan spinnerihärpäke tähän että suapi sen kielivalikon joskus jonnein elikkäs mitä luultavimmin tähän.
        Spinner spinneri = findViewById(id);
        ArrayList<String> kielilista = new ArrayList<>();
        kielilista.add("suomi");
        kielilista.add("English");
        kielilista.add("svenska");
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item,
                kielilista
        );
        spinneri.setAdapter(adapteri);
        spinneri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //LanguageHelper.changeLocale(resources), parent.getSelectedItem().toString();



                /*///// jtn shaisseeeeee

                if (parent.getSelectedItem().equals("suomi")) {
                    Paper.book().write("language", "fi");
                    updateView((String) Paper.book().read("language"));

                } else if (parent.getSelectedItem().equals("English")) {
                    Paper.book().write("language", "en");
                    updateView((String) Paper.book().read("language"));

                } else if (parent.getSelectedItem().equals("svenska")) {
                    Paper.book().write("language", "sv");
                    updateView((String) Paper.book().read("language"));

                }

                ///// */

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return spinneri;

    }

    public void onClick(View v) {
        AsetuksetOlio olio = AsetuksetOlio.getInstance();
        switch (v.getId()) {
            case R.id.tallennaNappi:
                olio.tallennaAsetukset(Integer.parseInt(fonttikokoKentta.getText().toString()), Integer.parseInt(leveysKentta.getText().toString()), Integer.parseInt(korkeusKentta.getText().toString()), Integer.parseInt(rivimaaraKentta.getText().toString()), muokkaus.isChecked(), displayTextSyote.getText().toString(), kieliValitsin.getSelectedItem().toString());
                siirryPaanakymaan();
                break;
            case R.id.peruutaNappi:
                siirryPaanakymaan();
                break;
        }

    }
}
