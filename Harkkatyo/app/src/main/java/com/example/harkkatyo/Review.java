package com.example.harkkatyo;

// Contains reviews information.

public class Review {

    private boolean showable = true;
    private String reviewID = null;
    private String userID = null;
    private String userName;
    private String meal;
    private String mealID;
    private String grade;
    private String verbal;
    private String date; // Note to myself: Consider using the date object if necessary.
    private String time;

    public Review(String rID, String uID, String uN, String m, String mID, String g, String v, String d, boolean s, String t) {
        reviewID = rID;
        userID = uID;
        userName = uN;
        meal = m;
        mealID = mID;
        grade = g;
        verbal = v;
        date = d;
        showable = s;
        time = t;
    }

    public Review(String rID, String uID, String uN, String m, String mID, String g, String v, String d, boolean s) {
        reviewID = rID;
        userID = uID;
        userName = uN;
        meal = m;
        mealID = mID;
        grade = g;
        verbal = v;
        date = d;
        showable = s;
    }

    public Review(String rID, String uID, String uN, String m, String mID, String g, String v, String d) {
        reviewID = rID;
        userID = uID;
        userName = uN;
        meal = m;
        mealID = mID;
        grade = g;
        verbal = v;
        date = d;
    }

    public Review(String uN, String m, String mID, String g, String v, String d) {
        userName = uN;
        meal = m;
        mealID = mID;
        grade = g;
        verbal = v;
        date = d;
    }

    public String getMeal() {
        return meal;
    }

    public String getGrade() {
        return grade;
    }

    public String getUserName() {
        return userName;
    }

    public String getVerbal() {
        return verbal;
    }

    public String getDate() {
        return date;
    }

    public String getReviewID() { return reviewID; }

    public String getUserID() {
        return userID;
    }

    public String getMealID() {
        return mealID;
    }

    public boolean isShowable() {
        return showable;
    }

    public String getShowableString() {
        if (showable) {
            return "1";
        } else {
            return "0";
        }
    }

    public String getTime() {
        return time;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public void setMealID(String mealID) {
        this.mealID = mealID;
    }

    public void setVerbal(String verbal) {
        this.verbal = verbal;
    }

    public void setShowableStatus(boolean showable) {
        this.showable = showable;
    }
}
