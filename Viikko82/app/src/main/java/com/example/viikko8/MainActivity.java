package com.example.viikko8;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tekstiNaytto;
    TextView liukuriOtsikko;
    TextView rahaMaaraKentta;
    TextView hintaKentta;
    Button lisaaRahaa;
    Button palautaRahat;
    Button ostaPullo;
    Button tulostaKuitti;
    SeekBar rahaLiukuri;
    String teksti;
    String juoma;
    Double koko;
    Spinner juomaValitsin;
    Spinner kokoValitsin;
    ArrayList<String> juomaLista;
    ArrayList<Double> kokoLista;
    Context context = null;


    BottleDispenser pullomaatti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = MainActivity.this;

        pullomaatti = BottleDispenser.getInstance();

        rahaMaaraKentta = findViewById(R.id.rahaMaaraKentta);
        rahaMaaraKentta.setText(String.format("%.2f€", pullomaatti.getMoney()));
        hintaKentta = findViewById(R.id.hintaKentta);

        juomaLista = getNimilista(pullomaatti.getBottle_array());
        kokoLista = getKokolista(pullomaatti.getBottle_array());

        tekstiNaytto = findViewById(R.id.tekstiNaytto);
        String teksti = "Tervetuloa! Tämä on pulloautomaatti tai joku semmoinen.\n\nOstettavissa olevat tuotteet:\n";
        pullomaatti.listaaPullot();
        for (Bottle pullo : pullomaatti.getBottle_array()) {
            teksti = teksti + pullo.getName() + " " + pullo.getSize() + " " + pullo.getCost() + "\n";
        }
        tekstiNaytto.setText(teksti);

        lisaaRahaa = findViewById(R.id.lisaaRahaaNappi);
        palautaRahat = findViewById(R.id.palautaRahatNappi);
        lisaaRahaa.setOnClickListener(this);
        palautaRahat.setOnClickListener(this);

        liukuriOtsikko = findViewById(R.id.liukuriOtsikko);
        rahaLiukuri = findViewById(R.id.rahaValitsin);
        rahaLiukuri.setProgress(0);
        liukuriOtsikko.setText("Valitse syötettävän rahan määrä: " + rahaLiukuri.getProgress() + "€");

        juomaValitsin = findViewById(R.id.juomaValitsin);
        kokoValitsin = findViewById(R.id.kokoValitsin);


        // Koon valitsemiseen käytettävä spinner:
        kokoValitsin = luoKokoSpinneri(R.id.kokoValitsin);

        // Juoman valitsemiseen käytettävä spinner:
        juomaValitsin = luoNimiSpinneri(R.id.juomaValitsin);


        hintaKentta.setText(String.format("%.2f€",((pullomaatti.etsiPullo(pullomaatti.getBottle_array(), juomaValitsin.getSelectedItem().toString(), (Double) kokoValitsin.getSelectedItem())).getCost())));






        rahaLiukuri.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                liukuriOtsikko.setText("Valitse syötettävän rahan määrä: " + progress + "€");
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public Spinner luoNimiSpinneri(int id) {
        Spinner spinneri = findViewById(id);;
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                getNimilista(pullomaatti.getBottle_array())
        );
        spinneri.setAdapter(adapteri);
        spinneri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                juoma = parent.getSelectedItem().toString();
                try {
                    hintaKentta.setText(String.format("%.2f€", ((pullomaatti.etsiPullo(pullomaatti.getBottle_array(), juomaValitsin.getSelectedItem().toString(), (Double) kokoValitsin.getSelectedItem())).getCost())));
                } catch (NullPointerException a) {
                    teksti = luoTeksti("Pulloa ei ole.");
                    tekstiNaytto.setText(teksti);
                }
                }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                juoma = null;
            }
        });
        kokoValitsin = luoKokoSpinneri(R.id.kokoValitsin);
        return spinneri;
    }

    public Spinner luoKokoSpinneri(int id) {
        Spinner spinneri = findViewById(id);;
        ArrayAdapter<Double> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                getKokolista(pullomaatti.getBottle_array())
        );
        spinneri.setAdapter(adapteri);
        spinneri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                koko = (Double) parent.getSelectedItem();
                try {
                    hintaKentta.setText(String.format("%.2f€", ((pullomaatti.etsiPullo(pullomaatti.getBottle_array(), juomaValitsin.getSelectedItem().toString(), (Double) kokoValitsin.getSelectedItem())).getCost())));
                } catch (NullPointerException a) {
                    teksti = luoTeksti("Pulloa ei ole.");
                    tekstiNaytto.setText(teksti);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                koko = null;
            }
        });
        return spinneri;
    }

    public ArrayList<Double> getKokolista(ArrayList<Bottle> pullolista, String nimi) {
        ArrayList<Double> kokolista = new ArrayList<>();
        if (nimi == null) {
            return getKokolista(pullomaatti.getBottle_array());
        }
        if (pullolista != null && pullolista.get(0).getName().equals(nimi)) {
            kokolista.add(pullolista.get(0).getSize());
        }
        for (Bottle pullo : pullolista) {
            if (!kokolista.contains(pullo.getSize()) && pullo.getName().equals(nimi)) {
                kokolista.add(pullo.getSize());
            }
        }
        return kokolista;
    }

    public ArrayList<Double> getKokolista(ArrayList<Bottle> pullolista) {
        ArrayList<Double> kokolista = new ArrayList<>();
        if (pullolista != null) {
            kokolista.add(pullolista.get(0).getSize());
        }
        for (Bottle pullo : pullolista) {
            if (!kokolista.contains(pullo.getSize())) {
                kokolista.add(pullo.getSize());
            }
        }
        return kokolista;
    }

    public ArrayList<String> getNimilista(ArrayList<Bottle> pullolista) {
        ArrayList<String> nimilista = new ArrayList<>();
        if (pullolista != null) {
            nimilista.add(pullolista.get(0).getName());
        }
        for (Bottle pullo : pullolista) {
            if (!nimilista.contains(pullo.getName())) {
                nimilista.add(pullo.getName());
            }
        }
        return nimilista;
    }

    public String luoTeksti(String text) {
        text = text + "\n\nOstettavissa olevat tuotteet:\n";
        for (Bottle pullo : pullomaatti.getBottle_array()) {
            text = text + pullo.getName() + " " + pullo.getSize() + " " + pullo.getCost() + "\n";
        }
        return text;
    }

    public String ostaPullo(String juoma, double koko) {
        ArrayList<Bottle> pullolista = pullomaatti.getBottle_array();
        for (Bottle pullo : pullolista) {
            if (pullo.getName().equals(juoma) && (pullo.getSize() == koko)) {
                return pullomaatti.buyBottle(juoma, koko);

            }

        }
        return "Pulloa ei löytynyt. Osta jokin muu pullo.";
    }


    public void writeFile(View v) {
        try {
            OutputStreamWriter ows = new OutputStreamWriter(context.openFileOutput("kuitti.txt", Context.MODE_PRIVATE));
            ows.write(pullomaatti.getViimeisinOsto().getName() + " " + pullomaatti.getViimeisinOsto().getCost());
            ows.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("Tiedosto kirjoitettu.");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ostaPulloNappi:
                String returni = pullomaatti.buyBottle(juoma, koko);
                teksti = luoTeksti(returni);
                tekstiNaytto.setText(teksti);
                juomaValitsin = luoNimiSpinneri(R.id.juomaValitsin);
                rahaMaaraKentta.setText(String.format("%.2f€", pullomaatti.getMoney()));
                break;
            case R.id.lisaaRahaaNappi:
                pullomaatti.addMoney(rahaLiukuri.getProgress());
                teksti = luoTeksti("Lisäsit laitteeseen " + rahaLiukuri.getProgress() + "€!\nRahaa on nyt " + pullomaatti.getMoney() + "€.");
                tekstiNaytto.setText(teksti);
                rahaMaaraKentta.setText(String.format("%.2f€", pullomaatti.getMoney()));
                rahaLiukuri.setProgress(0);
                pullomaatti.listaaPullot();
                break;
            case R.id.palautaRahatNappi:
                teksti = luoTeksti("Rahaa palautettiin " + String.format("%.2f", pullomaatti.getMoney()) + "€.");
                tekstiNaytto.setText(teksti);
                pullomaatti.returnMoney();
                rahaMaaraKentta.setText(String.format("%.2f€", pullomaatti.getMoney()));
                break;

            case R.id.tulostaKuittiNappi:
                try {
                    writeFile(tulostaKuitti);
                    teksti = luoTeksti("Kuitti kirjoitettu.");
                    tekstiNaytto.setText(teksti);
                } catch (Exception e) {
                    teksti = luoTeksti("Kuitin tulostaminen ei onnistunut.");
                    tekstiNaytto.setText(teksti);
                }
                break;
        }
    }



}
