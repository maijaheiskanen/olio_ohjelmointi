package com.example.viikko8;

public class Bottle {

    private int bottles;
    // The array for the Bottle-objects
    private Bottle[] bottle_array;
    public String nimi;
    public double koko;
    public String juoma;
    public double hinta;

    public Bottle() {
        koko = 0.5;
        nimi = "Pepsi Max";
        juoma = "Pepsi";
        hinta = 1.80;
    }

    public Bottle(String name, String drink, double tilavuus, double cost) {
        koko = tilavuus;
        nimi = name;
        juoma = drink;
        hinta = cost;
    }

    public String getName() {
        return nimi;
    }
    public String getDrink() {
        return juoma;
    }
    public double getSize() {
        return koko;
    }
    public double getCost() {
        return hinta;
    }
}


