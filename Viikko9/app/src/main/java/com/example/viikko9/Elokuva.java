package com.example.viikko9;

public class Elokuva {
    String nimi;
    String pvm;

    public Elokuva(String pNimi, String pPvm) {
        nimi = pNimi;
        pvm = pPvm;
    }

    public Elokuva(String pNimi) {
        nimi = pNimi;
        pvm = "01.01.1900";
    }

    public String getNimi() {
        return nimi;
    }
}
