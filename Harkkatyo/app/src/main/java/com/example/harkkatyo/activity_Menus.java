package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class activity_Menus extends AppCompatActivity {

    // TODO: CREATE A RESTAURANT SPINNER WHERE USER CAN CHOOSE WHICH RESTAURANTS MENUS THE USER WANTS TO VIEW!!!!!!!!!!!
    private String restaurantID = "1";

    private Spinner restaurantSpinner;

    private void loadActivity(Class where) {
        Intent intent = new Intent(this, where);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    // Bottom navigation menus controlling.
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_menus:
                    loadActivity(activity_Menus.class);
                    return true;
                case R.id.admin_navigation_reviews:
                    loadActivity(activity_Reviews.class);
                    return true;
                case R.id.navigation_newreview:
                    loadActivity(activity_NewReview.class);
                    return true;
                case R.id.admin_navigation_account:
                    loadActivity(activity_Account.class);
                    return true;
            }
            return false;
        }
    };

    /* Using Restaurant objects variable menuList, so this method is useless.
    private ArrayList<Menu> readMenusFromDB() {
        ArrayList<Menu> list = DatabaseSearcher.searchMenus(DatabaseInsert.getDatabase());
        return list;
    }
    */

    private void initRecyclerView(ArrayList<Menu> menuList) {
        RecyclerView reviewListing = findViewById(R.id.menusListing);
        RecyclerView.Adapter adapter = new MenuAdapter(this, menuList);
        reviewListing.setAdapter(adapter);
        reviewListing.setLayoutManager(new LinearLayoutManager(this));
        reviewListing.setHasFixedSize(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menus_activity);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Sets the restaurant spinner.
        restaurantSpinner = findViewById(R.id.menusRestaurantSpinner);
        restaurantSpinner = createRestaurantSpinner(R.id.menusRestaurantSpinner, University.getAvailableRestaurantList());
    }

    // Gets restaurant names in ArrayList and returns the string ArrayList. Used to create a spinner.
    private ArrayList<String> createRestaurantStringList(ArrayList<Restaurant> restaurantList) {
        ArrayList<String> restaurantStringList = new ArrayList<>();

        for (Restaurant r : restaurantList) {
            restaurantStringList.add(r.getName());
        }
        return restaurantStringList;
    }

    // Creates a spinner containing all restaurants.
    private Spinner createRestaurantSpinner(int id, ArrayList<Restaurant> restaurantList) {
        Spinner spinner = findViewById(id);
        ArrayList<String> restaurantStringList = createRestaurantStringList(restaurantList);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                restaurantStringList
        );
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Making the recycler view to list and show all wanted menus. Used this source to do: https://www.youtube.com/watch?v=Vyqz_-sJGFk

                // Finds the selected restaurants id.
                restaurantID = University.getRestaurantList().get(position).getID();
                // Updates the selected restaurants menuList.
                University.getRestaurant(restaurantID).updateMenulist();
                // Creates the RecyclerView with menuList.
                initRecyclerView(University.getRestaurant(restaurantID).getMenuList());
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // When nothing is selected, selects the first restaurant in the spinner.
                parent.setSelection(0);
                restaurantID = null;
            }
        });
        return spinner;
    }

}
