package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class activity_NewReview extends AppCompatActivity {
    private Spinner reviewSpinner, mealSpinner;
    private SeekBar gradeBar;
    private EditText verbalInput;

    private String meal, verbal;
    private int mealSpinnerProgress;
    private Review review;
    private int grade;

    private ArrayList<Review> reviewArrayList;
    private ArrayList<Meal> mealArrayList;

    // Tried to create this in own class so that this method could be called from there but didn't succeed, so now I have this method in every class.
    private void loadActivity(Class where) {
        Intent intent = new Intent(this, where);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    // Bottom navigation menus controlling.
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_menus:
                    loadActivity(activity_Menus.class);
                    return true;
                case R.id.admin_navigation_reviews:
                    loadActivity(activity_Reviews.class);
                    return true;
                case R.id.navigation_newreview:
                    loadActivity(activity_NewReview.class);
                    return true;
                case R.id.admin_navigation_account:
                    loadActivity(activity_Account.class);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_review_activity);

        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setSelectedItemId(R.id.navigation_newreview);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        reviewSpinner = findViewById(R.id.reviewSpinner);
        mealSpinner = findViewById(R.id.mealSpinner);
        gradeBar = findViewById(R.id.gradeBar);
        verbalInput = findViewById(R.id.verbalInput);

        // I thought this (finding all meals and reviews) would be easier by doing the database search than going through
        // University (the class) restaurantList and every Restaurants (class) mealList and every Meal (class) reviewList.

        // Searches already existing reviews from the database, belonging to the current user.
        reviewArrayList = DatabaseSearcher.searchReviewsByUserID(DatabaseInsert.getDatabase(), CurrentUser.getUserID());
        // Searches meals from the database.
        mealArrayList = DatabaseSearcher.searchMeals(DatabaseInsert.getDatabase());

        // Creating the spinners.
        reviewSpinner = createReviewSpinner(R.id.reviewSpinner, reviewArrayList);
        mealSpinner = createMealSpinner(R.id.mealSpinner, mealArrayList);
    }

    private boolean addInMeal() {
        String rID = reviewArrayList.get(reviewSpinner.getSelectedItemPosition()).getReviewID();
        try {
            for (Restaurant r : University.getRestaurantList()) {
                for (Meal m : r.getMealList()) {
                    for (Review x : m.getReviewList()) {
                        if (x.getReviewID().equals(rID)) {
                            x.setMealID(mealArrayList.get(mealSpinnerProgress).getId());
                            x.setMeal(mealArrayList.get(mealSpinnerProgress).getName());
                            x.setGrade(String.valueOf(gradeBar.getProgress()));
                            x.setVerbal(verbalInput.getText().toString());
                            break;
                        }
                    }
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveButton:
                // Creating a new review if there is no selected item in review spinner (or the title is selected).
                if (review == null) {
                    if (DatabaseInsert.insertReview(DatabaseInsert.getDatabase(), mealArrayList.get(mealSpinnerProgress).getId(), gradeBar.getProgress(), verbalInput.getText().toString(), CurrentUser.getUserID())) {
                        if (addInMeal()) {
                            Toast.makeText(this, "Review saved.",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(this, "Saving the review didn't succeed.",
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(this, "Saving the review didn't succeed.",
                                Toast.LENGTH_LONG).show();
                    }
                    // Updates already existing review if something was selected from the spinner.
                } else {
                    if (DatabaseUpdater.updateReview(DatabaseInsert.getDatabase(), review.getReviewID(), mealArrayList.get(mealSpinnerProgress).getId(), gradeBar.getProgress(), verbalInput.getText().toString())) {
                        Toast.makeText(this, "Review updated.",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(this, "Updating review doesn't succeed.",
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }

    }

    // If user selected the title (empty) from review spinner, clear the fields for creating a new review.
    public void clearFields() {
        mealSpinner = createMealSpinner(R.id.mealSpinner, mealArrayList);
        gradeBar.setProgress(1);
        verbalInput.setText("");
    }

    // Fills the EditText views with chosen reviews information.
    public void fillFields(Review r) {
        int i = 0;
        for (Meal m : mealArrayList) {
            if (m.getId().equals(r.getMealID())) {
                break;
            }
            i++;
        }
        mealSpinner.setSelection(i);
        gradeBar.setProgress(Integer.parseInt(r.getGrade()));
        verbalInput.setText(r.getVerbal());
    }


    public ArrayList<String> createReviewStringList(ArrayList<Review> reviewList) {
        ArrayList<String> stringList = new ArrayList<String>();
        for (Review r : reviewList) {
            stringList.add(r.getMeal() + " " + r.getDate());
        }
        return stringList;
    }

    public ArrayList<String> createMealStringList(ArrayList<Meal> mealList) {
        ArrayList<String> stringList = new ArrayList<String>();
        for (Meal m : mealList) {
            stringList.add(m.getName());
        }
        return stringList;
    }

    public Spinner createReviewSpinner(int id, ArrayList<Review> reviewList) {
        Spinner spinner = findViewById(id);
        ArrayList<String> reviewStringList = createReviewStringList(reviewList);
        reviewStringList.add(0, "Edit old review:");
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                reviewStringList
        );
        spinner.setAdapter(adapteri);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // If there is no old review selected, create new and save in the database.
                if (parent.getSelectedItem().equals("Edit old review:")) {
                    clearFields();
                    review = null;
                    // If an old review is selected, update selected reviews information in the database.
                } else {
                    String[] list = parent.getSelectedItem().toString().split(" ");
                    // Takes review with matching date (and also user, even though it doesn't show here).
                    review = DatabaseSearcher.searchReview(DatabaseInsert.getDatabase(), list[list.length - 1]);
                    fillFields(review);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                clearFields();
                review = null;

            }
        });
        return spinner;
    }

    public Spinner createMealSpinner(int id, ArrayList<Meal> mealList) {
        Spinner spinner = findViewById(id);
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                createMealStringList(mealList)
                );
        spinner.setAdapter(adapteri);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                meal = parent.getSelectedItem().toString();
                mealSpinnerProgress = parent.getSelectedItemPosition();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                parent.setSelection(1);
            }
        });
        return spinner;
    }


}
