package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class activityAdmin_userInformation extends AppCompatActivity {

    private String userID = null;
    private Spinner userSpinner;
    private ArrayList<User> userList = new ArrayList<>();
    private ArrayList<Review> reviewList = new ArrayList<>();
    private TextView nameOutput, emailOutput, userIDOutput, isAdminOutput;

    // Tried to create this in own class so that this method could be called from there but didn't succeed, so now I have this method in every class.
    private void loadActivity(Class where) {
        Intent intent = new Intent(this, where);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    // Bottom navigation view control.
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.admin_navigation_users:
                    loadActivity(activityAdmin_userInformation.class);
                    return true;
                case R.id.admin_navigation_restaurants:
                    loadActivity(activityAdmin_Restaurants.class);
                    return true;
                case R.id.admin_navigation_meals:
                    loadActivity(activityAdmin_Meals.class);
                    return true;
                case R.id.admin_navigation_reviews:
                    loadActivity(activityAdmin_Reviews.class);
                    return true;
                case R.id.admin_navigation_account:
                    loadActivity(activity_Account.class);
                    return true;
            }
            return false;
        }
    };

    private void initRecyclerView(ArrayList<Review> reviewList) {
        RecyclerView reviewListing = findViewById(R.id.adminReviewListing);
        RecyclerView.Adapter adapter = new AdminUsersReviewsAdapter(this, reviewList);
        reviewListing.setAdapter(adapter);
        reviewListing.setLayoutManager(new LinearLayoutManager(this));
        reviewListing.setHasFixedSize(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_user_information);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setSelectedItemId(R.id.admin_navigation_users);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        nameOutput = findViewById(R.id.userNameOutput);
        emailOutput = findViewById(R.id.userEmailOutput);
        userIDOutput = findViewById(R.id.userIDOutput);
        isAdminOutput = findViewById(R.id.isAdminOutput);

        // Search all user in the list.
        userList = DatabaseSearcher.searchUsers(DatabaseInsert.getDatabase());

        // Creating user spinner.
        userSpinner = findViewById(R.id.userSpinner);
        userSpinner = createUserSpinner(R.id.userSpinner, userList);

        initRecyclerView(reviewList);
    }

    // Creating a string ArrayList for the user spinner.
    public ArrayList<String> createUserStringList(ArrayList<User> userList) {
        ArrayList<String> userStringList = new ArrayList<>();
        for (User u : userList) {
            userStringList.add(u.getUserID() + " " + u.getEmail());
        }
        return userStringList;
    }

    // Creates a spinner containing all userIDs and user names.
    public Spinner createUserSpinner(int id, ArrayList<User> userList) {
        Spinner spinner = findViewById(id);
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                createUserStringList(userList)
        );
        spinner.setAdapter(adapteri);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userID = parent.getSelectedItem().toString().split(" ")[0];
                // Finds reviews only belonging to restaurants the current (admin) user has maintenance if.
                reviewList = DatabaseSearcher.searchReviewsByAdminID(DatabaseInsert.getDatabase(), userID, DatabaseSearcher.searchAdminsRestaurants(DatabaseInsert.getDatabase(), CurrentUser.getUserID()));
                initRecyclerView(reviewList);
                User user = DatabaseSearcher.searchUserInfo(DatabaseInsert.getDatabase(), userID);
                nameOutput.setText(user.getName());
                emailOutput.setText(user.getEmail());
                userIDOutput.setText(user.getUserID());
                if (user.getIsAdmin()) {
                    isAdminOutput.setText("True");
                } else {
                    isAdminOutput.setText("False");
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                parent.setSelection(0);
            }
        });
        return spinner;
    }

}
