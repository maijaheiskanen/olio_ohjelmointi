package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class activity_Account extends AppCompatActivity {
    private User userInfo;
    private EditText nameInput, emailInput, passwordInput, passwordAgainInput;

    // Tried to create this in own class so that this method could be called from there but didn't succeed, so now I have this method in every class.
    private void loadActivity(Class where) {
        Intent intent = new Intent(this, where);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    // Bottom navigation menus controlling.
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            // Checks if user is admin and creates the bottom menu depending on that.
            if (CurrentUser.getIsAdmin()) {
                switch (item.getItemId()) {
                    case R.id.admin_navigation_users:
                        loadActivity(activityAdmin_userInformation.class);
                        return true;
                    case R.id.admin_navigation_restaurants:
                        loadActivity(activityAdmin_Restaurants.class);
                        return true;
                    case R.id.admin_navigation_meals:
                        loadActivity(activityAdmin_Meals.class);
                        return true;
                    case R.id.admin_navigation_reviews:
                        loadActivity(activityAdmin_Reviews.class);
                        return true;
                    case R.id.admin_navigation_account:
                        loadActivity(activity_Account.class);
                        return true;
                }
            } else {
                switch (item.getItemId()) {
                    case R.id.navigation_menus:
                        loadActivity(activity_Menus.class);
                        return true;
                    case R.id.admin_navigation_reviews:
                        loadActivity(activity_Reviews.class);
                        return true;
                    case R.id.navigation_newreview:
                        loadActivity(activity_NewReview.class);
                        return true;
                    case R.id.admin_navigation_account:
                        loadActivity(activity_Account.class);
                        return true;
                }
            }
            return false;

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Checks if user is admin and creates the bottom menu depending on that.
        if (CurrentUser.getIsAdmin()) {
            setContentView(R.layout.activity_admin_account);
            BottomNavigationView navView = findViewById(R.id.admin_nav_view);
            navView.setSelectedItemId(R.id.admin_navigation_account);
            navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        } else {
            setContentView(R.layout.activity_account_activity);
            BottomNavigationView navView = findViewById(R.id.nav_view);
            navView.setSelectedItemId(R.id.admin_navigation_account);
            navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        }



        nameInput = findViewById(R.id.nameInput);
        emailInput = findViewById(R.id.emailInput);
        passwordInput = findViewById(R.id.passwordInput);
        passwordAgainInput = findViewById(R.id.passwordAgainInput);

        // Gets current users information so that it can be put in the text boxes to be edited by the user.
        // Doing this like this instead of using the CurrenUser object because CurrentUser doesn't contain users password.
        userInfo = DatabaseSearcher.searchUserInfo(DatabaseInsert.getDatabase(), CurrentUser.getUserID());

        // Just some debugging
        if (userInfo == null) {
            Toast.makeText(this, "userInfo is null",
                    Toast.LENGTH_LONG).show();
        }

        nameInput.setText(userInfo.getName());
        emailInput.setText(userInfo.getEmail());

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.logOutButton:
                System.out.println("Signed out.");


                CurrentUser.setUserID(null);
                Toast.makeText(this, "Logged out successfully.",
                        Toast.LENGTH_LONG).show();
                loadActivity(MainActivity.class);
                break;
            case R.id.saveButton:
                //TODO: Save edited information in the database.

                // First if checks if password fields are empty, in which case the password wont change
                if (passwordInput.length() == 0 && passwordAgainInput.length() == 0) {
                    System.out.println("Password fields were empty.");
                    userInfo = new User(CurrentUser.getUserID(), nameInput.getText().toString(), emailInput.getText().toString(), DatabaseSearcher.searchPasswordByUserID(DatabaseInsert.getDatabase(), CurrentUser.getUserID()), CurrentUser.getIsAdmin());
                    if (DatabaseUpdater.updateUser(DatabaseInsert.getDatabase(), userInfo)) {
                        Toast.makeText(this, "Account information saved successfully.",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(this, "Account information couldn't be saved.",
                                Toast.LENGTH_LONG).show();
                    }

                    // If password fields (or at least one of them) contain something, checks if the contents match
                } else {
                    if (passwordAgainInput.getText().toString().equals(passwordInput.getText().toString())) {
                        System.out.println("Password was correct.");
                        userInfo = new User(CurrentUser.getUserID(), nameInput.getText().toString(), emailInput.getText().toString(), passwordInput.getText().toString(), CurrentUser.getIsAdmin());

                        // Check if updating was successful or not and cast a toast depending on that
                        if (DatabaseUpdater.updateUser(DatabaseInsert.getDatabase(), userInfo)) {
                            Toast.makeText(this, "Account information saved successfully.",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(this, "Account information couldn't be saved.",
                                    Toast.LENGTH_LONG).show();
                        }
                    } else {
                        System.out.println("Password was wrong.");
                        Toast.makeText(this, "Password was wrong.",
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
            case R.id.saveXMLfileButton:
                // Save meals and reviews to XML file.
                if (FileWriter.writeStringInFile(this, "File.xml", XMLhelper.writeXML(this))) {
                    Toast.makeText(this, "XML file saved.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Couldn't save XML file.",
                            Toast.LENGTH_LONG).show();
                }

        }

    }

}
