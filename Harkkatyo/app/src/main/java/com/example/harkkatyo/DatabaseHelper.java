package com.example.harkkatyo;

// This class contains the methods to create the database.

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.harkkatyo.TableContract;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final  String DATABASE_NAME = "FoodReviews.db";
    private static final int DATABASE_VERSION = 1;

    public static String getDatabasesName() {
        return DATABASE_NAME;
    }

    public DatabaseHelper( Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Creating all the tables that belong to the database.

        final String SQL_CREATE_UNIVERSITY_TABLE = "CREATE TABLE " +
                TableContract.UniversityEntry.TABLE_NAME + " (" +
                TableContract.UniversityEntry.COLUMN_UNIVERSITYID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableContract.UniversityEntry.COLUMN_NAME +" VARCHAR(100) NOT NULL, " +
                TableContract.UniversityEntry.COLUMN_ADDRESS + " VARCHAR(100)" +
                ");";
        db.execSQL(SQL_CREATE_UNIVERSITY_TABLE);
        System.out.println("****\n*Table University created.\n****");

        /* I changed my mind and didn't want this table after all.
        final String SQL_CREATE_ADMIN_TABLE = "CREATE TABLE " +
                TableContract.AdminEntry.TABLE_NAME + " (" +
                TableContract.AdminEntry.COLUMN_ADMINID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableContract.AdminEntry.COLUMN_NAME +" VARCHAR(100) NOT NULL, " +
                TableContract.AdminEntry.COLUMN_EMAIL + " VARCHAR(100) NOT NULL, " +
                TableContract.AdminEntry.COLUMN_PASSWORD + " VARCHAR(20) NOT NULL CHECK(" + TableContract.AdminEntry.COLUMN_PASSWORD + " >= 4 AND length(" + TableContract.AdminEntry.COLUMN_PASSWORD + ") <= 20)" +
                ");";
        db.execSQL(SQL_CREATE_ADMIN_TABLE);
        System.out.println("****\n*Table Admin created.\n****");
        */

        final String SQL_CREATE_RESTAURANT_TABLE = "CREATE TABLE " +
                TableContract.RestaurantEntry.TABLE_NAME + " (" +
                TableContract.RestaurantEntry.COLUMN_RESTAURANTID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableContract.RestaurantEntry.COLUMN_UNIVERSITYID + " INTEGER NOT NULL, " +
                TableContract.RestaurantEntry.COLUMN_NAME +" VARCHAR(100) NOT NULL, " +
                TableContract.RestaurantEntry.COLUMN_ADDRESS + " VARCHAR(100) NOT NULL, " +
                TableContract.RestaurantEntry.COLUMN_AVAILABLE + " INTEGER DEFAULT 1 CHECK (" + TableContract.RestaurantEntry.COLUMN_AVAILABLE + "> -1 AND " + TableContract.RestaurantEntry.COLUMN_AVAILABLE + " < 2), " +
                "FOREIGN KEY(" + TableContract.RestaurantEntry.COLUMN_UNIVERSITYID + ") REFERENCES " + TableContract.UniversityEntry.TABLE_NAME + "( " + TableContract.UniversityEntry.COLUMN_UNIVERSITYID + ")" +
                ");";
        db.execSQL(SQL_CREATE_RESTAURANT_TABLE);
        System.out.println("****\n*Table Restaurant created.\n****");

        final String SQL_CREATE_MAINTENANCE_TABLE = "CREATE TABLE " +
                TableContract.MaintenanceEntry.TABLE_NAME + " (" +
                TableContract.MaintenanceEntry.COLUMN_USERID + " INTEGER NOT NULL, " +
                TableContract.MaintenanceEntry.COLUMN_RESTAURANTID +" NOT NULL, " +
                "PRIMARY KEY (" + TableContract.MaintenanceEntry.COLUMN_USERID + ", " + TableContract.MaintenanceEntry.COLUMN_RESTAURANTID + ")," +
                "FOREIGN KEY(" + TableContract.MaintenanceEntry.COLUMN_USERID + ") REFERENCES " + TableContract.UserEntry.TABLE_NAME + "( " + TableContract.UserEntry.COLUMN_USERID + ")" + " ON DELETE CASCADE, " +
                "FOREIGN KEY(" + TableContract.MaintenanceEntry.COLUMN_RESTAURANTID + ") REFERENCES " + TableContract.RestaurantEntry.TABLE_NAME + "( " + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + ")" + " ON DELETE CASCADE " +
                ");";
        db.execSQL(SQL_CREATE_MAINTENANCE_TABLE);
        System.out.println("****\n*Table Maintenance created.\n****");

        final String SQL_CREATE_MEAL_TABLE = "CREATE TABLE " +
                TableContract.MealEntry.TABLE_NAME + " (" +
                TableContract.MealEntry.COLUMN_MEALID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableContract.MealEntry.COLUMN_RESTAURANTID + " INTEGER NOT NULL, " +
                TableContract.MealEntry.COLUMN_NAME +" VARCHAR(100) NOT NULL, " +
                TableContract.MealEntry.COLUMN_TYPE + " INTEGER NOT NULL CHECK(" + TableContract.MealEntry.COLUMN_TYPE + " > 0 AND " + TableContract.MealEntry.COLUMN_TYPE + " < 4)," +
                TableContract.MealEntry.COLUMN_LACTOSEFREE + " BOOLEAN DEFAULT 0," +
                TableContract.MealEntry.COLUMN_GLUTENFREE + " BOOLEAN DEFAULT 0," +
                "FOREIGN KEY(" + TableContract.MealEntry.COLUMN_RESTAURANTID + ") REFERENCES " + TableContract.RestaurantEntry.TABLE_NAME + "( " + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + ")" + " ON DELETE CASCADE " +
                ");";
        db.execSQL(SQL_CREATE_MEAL_TABLE);
        System.out.println("****\n*Table Meal created.\n****");

        final String SQL_CREATE_MENU_TABLE = "CREATE TABLE " +
                TableContract.MenuEntry.TABLE_NAME + " (" +
                TableContract.MenuEntry.COLUMN_MENUID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableContract.MenuEntry.COLUMN_RESTAURANTID + " INTEGER NOT NULL, " +
                "FOREIGN KEY(" + TableContract.MenuEntry.COLUMN_RESTAURANTID + ") REFERENCES " + TableContract.RestaurantEntry.TABLE_NAME + "( " + TableContract.RestaurantEntry.COLUMN_RESTAURANTID + ")" + " ON DELETE CASCADE " +
                ");";
        db.execSQL(SQL_CREATE_MENU_TABLE);
        System.out.println("****\n*Table Menu created.\n****");

        /* I changed my mind and didn't want this table after all.
        final String SQL_CREATE_DATE_TABLE = "CREATE TABLE " +
                TableContract.DateEntry.TABLE_NAME + " (" +
                TableContract.DateEntry.COLUMN_DATE + " DATE PRIMARY KEY " +
                ");";
        db.execSQL(SQL_CREATE_DATE_TABLE);
        System.out.println("****\n*Table Date created.\n****");
        */

        final String SQL_CREATE_HASDATE_TABLE = "CREATE TABLE " +
                TableContract.HasDateEntry.TABLE_NAME + " (" +
                TableContract.HasDateEntry.COLUMN_DATE + " DATE NOT NULL, " +
                TableContract.HasDateEntry.COLUMN_MENUID +" INTEGER NOT NULL, " +
                "PRIMARY KEY (" + TableContract.HasDateEntry.COLUMN_DATE + ", " + TableContract.HasDateEntry.COLUMN_MENUID + ")," +
                "FOREIGN KEY(" + TableContract.HasDateEntry.COLUMN_MENUID + ") REFERENCES " + TableContract.MenuEntry.TABLE_NAME + "( " + TableContract.MenuEntry.COLUMN_MENUID+ ")" + " ON DELETE CASCADE " +
                ");";
        db.execSQL(SQL_CREATE_HASDATE_TABLE);
        System.out.println("****\n*Table HasDate created.\n****");

        final String SQL_CREATE_ONLIST_TABLE = "CREATE TABLE " +
                TableContract.OnListEntry.TABLE_NAME + " (" +
                TableContract.OnListEntry.COLUMN_MEALID + " INTEGER NOT NULL, " +
                TableContract.OnListEntry.COLUMN_MENUID +" INTEGER NOT NULL, " +
                "PRIMARY KEY (" + TableContract.OnListEntry.COLUMN_MEALID + ", " + TableContract.HasDateEntry.COLUMN_MENUID + ")," +
                "FOREIGN KEY(" + TableContract.OnListEntry.COLUMN_MEALID + ") REFERENCES " + TableContract.MealEntry.TABLE_NAME + "( " + TableContract.MealEntry.COLUMN_MEALID + ")" + " ON DELETE CASCADE, " +
                "FOREIGN KEY(" + TableContract.OnListEntry.COLUMN_MENUID + ") REFERENCES " + TableContract.MenuEntry.TABLE_NAME + "( " + TableContract.MenuEntry.COLUMN_MENUID+ ")" + " ON DELETE CASCADE " +
                ");";
        db.execSQL(SQL_CREATE_ONLIST_TABLE);
        System.out.println("****\n*Table OnList created.\n****");

        final String SQL_CREATE_USER_TABLE = "CREATE TABLE " +
                TableContract.UserEntry.TABLE_NAME + " (" +
                TableContract.UserEntry.COLUMN_USERID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableContract.UserEntry.COLUMN_NAME +" VARCHAR(100) NOT NULL, " +
                TableContract.UserEntry.COLUMN_EMAIL + " VARCHAR(100) UNIQUE NOT NULL, " +
                TableContract.UserEntry.COLUMN_PASSWORD + " VARCHAR(20) NOT NULL CHECK(length(" + TableContract.UserEntry.COLUMN_PASSWORD + ") >= 4 AND length(" + TableContract.UserEntry.COLUMN_PASSWORD + ") <= 20)," +
                TableContract.UserEntry.COLUMN_ISADMIN + " INTEGER NOT NULL CHECK (" + TableContract.UserEntry.COLUMN_ISADMIN + "> -1 AND " + TableContract.UserEntry.COLUMN_ISADMIN + " < 2) " +
                ");";
        db.execSQL(SQL_CREATE_USER_TABLE);
        System.out.println("****\n*Table User created.\n****");

        final String SQL_CREATE_REVIEW_TABLE = "CREATE TABLE " +
                TableContract.ReviewEntry.TABLE_NAME + " (" +
                TableContract.ReviewEntry.COLUMN_REVIEWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TableContract.ReviewEntry.COLUMN_USERID + " INTEGER NOT NULL, " +
                TableContract.ReviewEntry.COLUMN_MEALID +" INTEGER NOT NULL, " +
                TableContract.ReviewEntry.COLUMN_GARDE + " INTEGER NOT NULL CHECK (" + TableContract.ReviewEntry.COLUMN_GARDE + "> 0 AND " + TableContract.ReviewEntry.COLUMN_GARDE + " < 6), " +
                TableContract.ReviewEntry.COLUMN_VERBALEVALUATION + " VARCHAR(200), " +
                TableContract.ReviewEntry.COLUMN_POSOTOVEVOTE + " INTEGER DEFAULT 0, " +
                TableContract.ReviewEntry.COLUMN_NEGATIVEVOTE + " INTEGER DEFAULT 0, " +
                TableContract.ReviewEntry.COLUMN_DATE + " DATE NOT NULL, " +
                TableContract.ReviewEntry.COLUMN_TIME + " TIME NOT NULL, " +
                TableContract.ReviewEntry.COLUMN_SHOWABLE + " INTEGER DEFAULT 1 CHECK (" + TableContract.ReviewEntry.COLUMN_SHOWABLE + "> -1 AND " + TableContract.ReviewEntry.COLUMN_SHOWABLE + " < 2), " +
                "FOREIGN KEY(" + TableContract.ReviewEntry.COLUMN_USERID + ") REFERENCES " + TableContract.UserEntry.TABLE_NAME + "( " + TableContract.UserEntry.COLUMN_USERID + ")," +
                "FOREIGN KEY(" + TableContract.ReviewEntry.COLUMN_MEALID+ ") REFERENCES " + TableContract.MealEntry.TABLE_NAME + "( " + TableContract.MealEntry.COLUMN_MEALID+ ")" + " ON DELETE CASCADE " +
                ");";
        db.execSQL(SQL_CREATE_REVIEW_TABLE);
        System.out.println("****\n*Table Review created.\n****");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TableContract.UniversityEntry.TABLE_NAME);
        //db.execSQL("DROP TABLE IF EXISTS " + TableContract.AdminEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableContract.RestaurantEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableContract.MaintenanceEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableContract.MealEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableContract.MenuEntry.TABLE_NAME);
        //db.execSQL("DROP TABLE IF EXISTS " + TableContract.DateEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableContract.HasDateEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableContract.OnListEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableContract.UserEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TableContract.ReviewEntry.TABLE_NAME);
    }
}
