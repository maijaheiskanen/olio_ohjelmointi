package com.example.harkkatyo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<com.example.harkkatyo.Menu> mMenuList = new ArrayList<>();

    public MenuAdapter(Context context, ArrayList<Menu> menuList) {
        mMenuList = menuList;
        mContext = context;
    }

    @NonNull
    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_menu_list_item, viewGroup, false);
        MenuAdapter.ViewHolder holder = new MenuAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.ViewHolder viewHolder, int i) {
        viewHolder.restaurant.setText(mMenuList.get(i).getRestaurant());
        viewHolder.date.setText((mMenuList.get(i).getDate()));
        viewHolder.kotoisiaMakuja.setText((mMenuList.get(i).getKotoisiaMakuja()));
        viewHolder.kokinSuositus.setText((mMenuList.get(i).getKokinSuositus()));
        viewHolder.kasvisherkkuja.setText((mMenuList.get(i).getKasvisherkkuja()));

        viewHolder.menuListItemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "You clicked this menu, but nothing happened :(",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMenuList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView restaurant, kotoisiaMakuja, kokinSuositus, kasvisherkkuja, date;
        private LinearLayout menuListItemLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            restaurant = itemView.findViewById(R.id.restaurantName);
            date = itemView.findViewById(R.id.dateOutput);
            kotoisiaMakuja = itemView.findViewById(R.id.kotoisiaMakujaOutput);
            kokinSuositus = itemView.findViewById(R.id.kokinSuositusOutput);
            kasvisherkkuja = itemView.findViewById(R.id.kasvisherkkujaOutput);
            menuListItemLayout = itemView.findViewById(R.id.menu_list_item_layout);
        }
    }
}


