package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;


//TODO: Make the recycler views layout more clear (add some title ect.)

public class activity_Reviews extends AppCompatActivity {

    // A list where all the wanted menus are been read from the database
    private ArrayList<Review> reviewList = new ArrayList<>();

    // Tried to create this in own class so that this method could be called from there but didn't succeed, so now I have this method in every class.
    private void loadActivity(Class where) {
        Intent intent = new Intent(this, where);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    // Managing the bottom navigation menu so that depending on clicked icon the wanted activity is loaded.
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_menus:
                    loadActivity(activity_Menus.class);
                    return true;
                case R.id.admin_navigation_reviews:
                    loadActivity(activity_Reviews.class);
                    return true;
                case R.id.navigation_newreview:
                    loadActivity(activity_NewReview.class);
                    return true;
                case R.id.admin_navigation_account:
                    loadActivity(activity_Account.class);
                    return true;
            }
            return false;
        }
    };

    private ArrayList<Review> readReviewsFromDB() {
        ArrayList<Review> list = new ArrayList<>();
        list = DatabaseSearcher.searchReviews(DatabaseInsert.getDatabase());
        return list;
    }

    // Puts the reviews (from the list given as parameter) in the recycler view
    private void initRecyclerView(ArrayList<Review> reviewList) {
        RecyclerView reviewListing = findViewById(R.id.adminReviewListing);
        RecyclerView.Adapter adapter = new ReviewAdapter(this, reviewList);
        reviewListing.setAdapter(adapter);
        reviewListing.setLayoutManager(new LinearLayoutManager(this));
        reviewListing.setHasFixedSize(true);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews_activity);

        // Setting the bottom navigation bar to function.
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setSelectedItemId(R.id.admin_navigation_reviews);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Making the recycler view to list and show all wanted menus. Used this source to do: https://www.youtube.com/watch?v=Vyqz_-sJGFk
        reviewList = readReviewsFromDB();
        initRecyclerView(reviewList);



    }

}
