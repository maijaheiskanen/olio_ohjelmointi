package com.example.harkkatyo;

// Contains menus information.

public class Menu {
    private String menuID, restaurantID, restaurant, date, kotoisiaMakuja = null, kokinSuositus = null, kasvisherkkuja = null;

    public Menu(String mID, String rID, String r, String d) {
        menuID = mID;
        restaurantID = rID;
        restaurant = r;
        date = d;
    }

    public void setKotoisiaMakuja(String kM) {
        kotoisiaMakuja = kM;
    }

    public void setKasvisherkkuja(String kH) {
        kasvisherkkuja = kH;
    }

    public void setKokinSuositus(String kS) {
        kokinSuositus = kS;
    }

    public String getRestaurant() {
        return restaurant;
    }

    public String getRestaurantID() {
        return restaurantID;
    }

    public String getKotoisiaMakuja() {
        return kotoisiaMakuja;
    }

    public String getKokinSuositus() {
        return kokinSuositus;
    }

    public String getKasvisherkkuja() {
        return kasvisherkkuja;
    }

    public String getDate() {
        return date;
    }

    public String getMenuID() {
        return menuID;
    }
}
