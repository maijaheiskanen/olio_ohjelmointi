package com.example.viikko11_3;

public class AsetuksetOlio {
    static private int fonttikoko;
    static private int leveys;
    static private int korkeus;
    static private int rivimaara;
    static private boolean muokkaus;
    static private String teksti;
    static private String muokattavaTeksti;
    static private String displayText;
    static private String kieli;

    private static AsetuksetOlio olio = new AsetuksetOlio();

    private AsetuksetOlio() {
        fonttikoko = 14;
        leveys = 300;
        korkeus = 100;
        rivimaara = 4;
        muokkaus = false;
        teksti = "Kirjoita tekstisi tähän...";
        muokattavaTeksti = "Kirjoita tekstisi tähän...";
        displayText = " ";
        kieli = "fi";
    }

    public static AsetuksetOlio getInstance() {
        return olio;
    }

    public void tallennaAsetukset(int a, int b, int c, int d, boolean e, String f, String g) {
        this.fonttikoko = a;
        this.leveys = b;
        this.korkeus = c;
        this.rivimaara = d;
        this.muokkaus = e;
        displayText = f;
        kieli = g;
    }

    public void setTeksti(String s) {
        teksti = s;
    }

    public void setMuokattavaTeksti(String s) {
        muokattavaTeksti = s;
    }

    public int getFonttikoko() {
        return fonttikoko;
    }

    public int getLeveys() {
        return leveys;
    }

    public int getKorkeus() {
        return korkeus;
    }

    public int getRivimaara() {
        return rivimaara;
    }

    public boolean getMuokkaus() {
        return muokkaus;
    }

    public String getTeksti() {
        return teksti;
    }

    public String getMuokattavaTeksti() {
        return muokattavaTeksti;
    }

    public String getDisplayText() {
        return displayText;
    }

    public String getKieli() {
        return kieli;
    }
}
