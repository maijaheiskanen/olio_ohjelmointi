package com.example.harkkatyo;

// Contain users information.
// Is it smart to have users password as a variable in this class, I don't know.
// For some already forgotten reason I decided that I need the password here.

public class User {
    private String userID, name, email, password;
    private boolean isAdmin;

    public User(String uID, String n, String e) {
        userID = uID;
        name = n;
        email = e;
        password = null;
        isAdmin = false;
    }

    public User(String uID, String n, String e, String p) {
        userID = uID;
        name = n;
        email = e;
        password = p;
        isAdmin = false;
    }

    public User(String uID, String n, String e, String p, boolean iA) {
        userID = uID;
        name = n;
        email = e;
        password = p;
        isAdmin = iA;
    }

    public String getUserID() {
        return userID;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }
}
