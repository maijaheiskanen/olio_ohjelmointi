package com.example.viikko9;

import android.content.Context;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {

    Context context = null;
    Spinner teatteriValitsin;
    Spinner elokuvaValitsin;
    Spinner pvmValitsin;
    EditText aloitusAikaSyote;
    EditText lopetusAikaSyote;
    ScrollView listaus;
    TextView tulosKentta;

    Finnkino finnkino = Finnkino.getInstance();
    String paikka;
    String elokuva;
    String pvm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Luetaan teatterit, jotta niita voidaan kayttaa heti alusta asti.
        finnkino.teatteritReadXML();
        finnkino.elokuvatReadXML();
        finnkino.pvmReadXML();

        teatteriValitsin = findViewById(R.id.teatteriValitsin);
        teatteriValitsin = luoTeatteriSpinneri(R.id.teatteriValitsin);
        elokuvaValitsin = findViewById(R.id.elokuvaValitsin);
        elokuvaValitsin = luoElokuvaSpinneri(R.id.elokuvaValitsin);
        pvmValitsin = findViewById(R.id.pvmValitsin);
        pvmValitsin = luoPvmSpinneri(R.id.pvmValitsin);

        aloitusAikaSyote = findViewById(R.id.aloitusAikaSyote);
        lopetusAikaSyote = findViewById(R.id.lopetusAikaSyote);

        listaus = findViewById(R.id.listaus);
        tulosKentta = findViewById(R.id.skrollinTekstikentta);
        tulosKentta.setText("Ei tuloksia.");

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd");
        pvm = df.format(c);
        paikka = "Kaikki";


    }




    // Luodaan spinnereita.
    public Spinner luoTeatteriSpinneri(int id) {
        Spinner spinneri = findViewById(id);;
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                finnkino.getTeatteriNimilista()
        );
        spinneri.setAdapter(adapteri);
        spinneri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                paikka = parent.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                paikka = "Kaikki";
            }
        });
        return spinneri;
    }
    public Spinner luoElokuvaSpinneri(int id) {
        Spinner spinneri = findViewById(id);;
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                finnkino.getElokuvaNimilista()
        );
        spinneri.setAdapter(adapteri);
        spinneri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                elokuva = parent.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                elokuva = "Kaikki";
            }
        });
        return spinneri;
    }
    public Spinner luoPvmSpinneri(int id) {
        Spinner spinneri = findViewById(id);;
        ArrayAdapter<String> adapteri = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_item ,
                finnkino.getPvmLista()
        );
        spinneri.setAdapter(adapteri);
        spinneri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pvm = parent.getSelectedItem().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MMM-dd");
                pvm = df.format(c);
            }
        });
        return spinneri;
    }

    //@Override
    public void onClick(View v) throws ParseException {
        switch (v.getId()) {
            case R.id.haeNappi:
                //TODO: Valintarakenne, jolla tarkistetaan mita tietoja on annettu ja sen perusteella suoritetaan tarvittavat haut.
                if (!pvm.equals("Kaikki") && !paikka.equals("Kaikki")) {

                }
                tulosKentta.setText(finnkino.elokuvatReadXML(paikka, elokuva, pvm, aloitusAikaSyote.getText().toString(), lopetusAikaSyote.getText().toString()));
                //tulosKentta.setText(finnkino.elokuvatReadXML(teatteriValitsin.getSelectedItem().toString(), elokuvaValitsin.getSelectedItem().toString(), pvmValitsin.getSelectedItem().toString().split("T")[0], aloitusAikaSyote.getText().toString(), lopetusAikaSyote.getText().toString()));
                //tulosKentta.setText("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

                break;
        }
    }

}
