package com.example.harkkatyo;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;

public class activityAdmin_Reviews extends AppCompatActivity {

    // Tried to create this in own class so that this method could be called from there but didn't succeed, so now I have this method in every class.
    private void loadActivity(Class where) {
        Intent intent = new Intent(this, where);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    // Bottom navigation view control.
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.admin_navigation_users:
                    loadActivity(activityAdmin_userInformation.class);
                    return true;
                case R.id.admin_navigation_restaurants:
                    loadActivity(activityAdmin_Restaurants.class);
                    return true;
                case R.id.admin_navigation_meals:
                    loadActivity(activityAdmin_Meals.class);
                    return true;
                case R.id.admin_navigation_reviews:
                    loadActivity(activityAdmin_Reviews.class);
                    return true;
                case R.id.admin_navigation_account:
                    loadActivity(activity_Account.class);
                    return true;
            }
            return false;
        }
    };

    private void initRecyclerView(ArrayList<Review> reviewList) {
        RecyclerView reviewListing = findViewById(R.id.adminReviewListing);
        RecyclerView.Adapter adapter = new AdminReviewsAdapter(this, reviewList);
        reviewListing.setAdapter(adapter);
        reviewListing.setLayoutManager(new LinearLayoutManager(this));
        reviewListing.setHasFixedSize(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin__reviews);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setSelectedItemId(R.id.admin_navigation_reviews);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Creates RecyclerView with only reviews that belong to restaurants the current (admin) user has maintenance of.
        // (So the admin cant't for example hide competitive restaurants reviews etc.)
        initRecyclerView(DatabaseSearcher.searchReviewsAdmin(DatabaseInsert.getDatabase(), DatabaseSearcher.searchAdminsRestaurants(DatabaseInsert.getDatabase(), CurrentUser.getUserID())));
    }

}
