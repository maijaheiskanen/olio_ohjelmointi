package com.example.viikko11_3;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.TypedValue;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

@SuppressWarnings("ALL")
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView tekstiRuutu, displayText;
    EditText muokattavaTeksti;
    String jarkko;

    //TODO: https://www.youtube.com/watch?v=ywF-ySiBAsc kielenvalitsemisjuttuun video, vähän kyseenalainen enkä ymmärrä mitä siinä tapahtuu, mutta jos ei muuta keksi.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        AsetuksetOlio olio = AsetuksetOlio.getInstance();

        jarkko = getIntent().getStringExtra("jarkko");
        if (jarkko == null) {
            jarkko = "fi";
        }
        if (!jarkko.equals(olio.getKieli())) {
            changeLocale(olio.getKieli());
        }

        displayText = findViewById(R.id.displayText);
        displayText.setText(olio.getDisplayText());

        tekstiRuutu = findViewById(R.id.tekstiRuutu);
        if (olio == null) {
            System.out.println("kele tana tu");
        } else if (tekstiRuutu == null) {
            System.out.println("voehan sviddunääs");

        }
        tekstiRuutu.setTextSize(TypedValue.COMPLEX_UNIT_DIP, olio.getFonttikoko());
        tekstiRuutu.getLayoutParams().height = olio.getKorkeus();
        tekstiRuutu.getLayoutParams().width = olio.getLeveys();
        tekstiRuutu.setLines(olio.getRivimaara());

        muokattavaTeksti = findViewById(R.id.muokattavaTeksti);

        muokattavaTeksti.setEnabled(!olio.getMuokkaus());
        muokattavaTeksti.setText(olio.getMuokattavaTeksti());

        if (olio.getMuokkaus()) {
            olio.setTeksti(muokattavaTeksti.getText().toString());
            tekstiRuutu.setText(olio.getTeksti());
        }



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void siirryAsetuksiin() {
        Intent intent = new Intent(MainActivity.this, Asetukset.class);
        startActivity(intent);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        AsetuksetOlio olio = AsetuksetOlio.getInstance();
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_tools) {
            System.out.println("Päästiin tähän.");
            olio.setMuokattavaTeksti(muokattavaTeksti.getText().toString());
            siirryAsetuksiin();

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void changeLocale(String locale) {
        Resources res = getResources();
        Configuration config;
        config = new Configuration(res.getConfiguration());

        switch (locale) {
            case "suomi":
                config.setLocale(new Locale("fi"));
                jarkko = "suomi";
                break;
            case "English":
                config.setLocale(new Locale("en"));
                jarkko = "English";
                break;
            case "svenska":
                config.setLocale(new Locale("sv"));
                jarkko = "svenska";
                break;

        }
        finish();
        res.updateConfiguration(config, res.getDisplayMetrics());
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        intent.putExtra("jarkko", jarkko);
        startActivity(intent);
    }
}
