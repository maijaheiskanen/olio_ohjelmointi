package com.example.viikko8;

import java.util.ArrayList;
import java.util.Scanner;

public class BottleDispenser {

    private int bottles = 6;
    private static BottleDispenser pullomaatti = new BottleDispenser();
    private ArrayList<Bottle> bottle_array = new ArrayList();
    private double money;
    private Bottle viimeisinOsto;

    private BottleDispenser() {
        Bottle temp;
        for(int i = 0; i < 6; i++) {
            // Use the default constructor to create new Bottles
            if (i == 0) {
                temp = new Bottle();
            } else if (i == 1) {
                temp = new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2);
            } else if (i == 2) {
                temp = new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0);
            } else if (i == 3) {
                temp = new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5);
            } else if (i == 4 || i == 5) {
                temp = new Bottle("Fanta Zero", "Fanta", 0.5, 1.95);
            } else {
                temp = new Bottle();
            }
            bottle_array.add(temp);
            bottles += 1;
        }
    }

    public static BottleDispenser getInstance() {
        return pullomaatti;
    }

    public void luo_pullot() {
        Bottle temp;
        for(int i = 0; i < bottles; i++) {
            // Use the default constructor to create new Bottles
            if (i==0) {
                temp = new Bottle();
            } else if (i==1) {
                temp = new Bottle("Pepsi Max", "Pepsi", 1.5, 2.2);
            } else if (i==2) {
                temp = new Bottle("Coca-Cola Zero", "Coca-Cola", 0.5, 2.0);
            } else if (i==3) {
                temp = new Bottle("Coca-Cola Zero", "Coca-Cola", 1.5, 2.5);
            } else if (i==4 || i==5) {
                temp = new Bottle("Fanta Zero", "Fanta", 0.5, 1.95);
            } else {
                temp = new Bottle();
            }

            this.bottle_array.add(temp);
        }
    }


    public void addMoney(int raha) {
        money += raha;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    private void deleteBottle(int index) {
        bottle_array.remove(bottle_array.get(index));
    }

    public String buyBottle(String juoma, double koko) { //TODO: vaihda toimimaan juoman nimellä ja koolla indeksin sisään
        for (Bottle pullo : bottle_array) {
            if (pullo.getName().equals(juoma) && (pullo.getSize() == koko)) {
                if (money < pullo.getCost()) {
                    return ("Rahasi eivät riitä. Lisää " + (pullo.getCost() - money) + "€.");
                } else if (bottles == 0) {
                    return("Pullot loppuivat!");
                } else {
                    bottles -= 1;
                    money -= pullo.getCost();
                    deleteBottle(bottle_array.indexOf(pullo)); //
                    viimeisinOsto = pullo;
                    return ("KACHUNK! " + pullo.getName() + " tipahti masiinasta!");
                }
            }
        }
        return "Tuote on loppu.";
    }

    public Bottle etsiPullo(ArrayList<Bottle> pullolista, String juoma, double koko) {
        for (Bottle pullo : pullolista) {
            if (pullo.getName().equals(juoma) && (pullo.getSize() == koko)) {
                return pullo;
            }

        }
        return null;
    }

    public void returnMoney() {
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + String.format("%.2f", money) + "€");
        money = 0;
    }

    public double getMoney() {
        return money;
    }

    public ArrayList<Bottle> getBottle_array() {
        return bottle_array;
    }

    public Bottle getViimeisinOsto() {
        return viimeisinOsto;
    }

    /**
     *
     */
    public void ostaPullo() {
        for (int i = 0; i < (bottle_array.size()); i++) {
            System.out.println(i+1 + ". Nimi: " + bottle_array.get(i).getName());
            System.out.println('\t' + "Koko: " + bottle_array.get(i).getSize() + '\t' + "Hinta: " + bottle_array.get(i).getCost());
        }
        System.out.print("Valintasi: ");
        Scanner scan = new Scanner(System.in);
        int valinta = scan.nextInt();
        //buyBottle(valinta-1);
    }

    public void listaaPullot() {
        System.out.println("Listassa olevat pullot: ");
        for (int i = 0; i < (bottle_array.size()); i++) {
            System.out.println(i+1 + ". Nimi: " + bottle_array.get(i).getName());
            System.out.println('\t' + "Koko: " + bottle_array.get(i).getSize() + '\t' + "Hinta: " + bottle_array.get(i).getCost());
        }
    }
}
