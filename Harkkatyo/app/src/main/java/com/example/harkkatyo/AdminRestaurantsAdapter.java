package com.example.harkkatyo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AdminRestaurantsAdapter extends RecyclerView.Adapter<AdminRestaurantsAdapter.ViewHolder>{

    private Context mContext;
    private ArrayList<Restaurant> mRestaurantList = new ArrayList<>();

    public AdminRestaurantsAdapter(Context context, ArrayList<Restaurant> restaurantList) {
        mRestaurantList = restaurantList;
        mContext = context;
    }

    @NonNull
    @Override
    public AdminRestaurantsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_admin_restaurants_list_item, viewGroup, false);
        AdminRestaurantsAdapter.ViewHolder holder = new AdminRestaurantsAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AdminRestaurantsAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.name.setText(mRestaurantList.get(i).getName());
        viewHolder.address.setText(mRestaurantList.get(i).getAddress());
        viewHolder.university.setText(mRestaurantList.get(i).getUniversityName());


        // Sets delete button and switch clickable depending on if admin has maintenance of restaurant.
        University.updateAdminRestaurantList();
        if (!University.getAdminRestaurantList().contains(mRestaurantList.get(i).getID())) {
            viewHolder.deleteButton.setEnabled(false);
            viewHolder.deleteButton.setBackgroundColor(0x00fffff);
            viewHolder.availableSwitch.setEnabled(false);
        }
        viewHolder.availableSwitch.setChecked(mRestaurantList.get(i).isAvailable());

        viewHolder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Setting the button not clickable after first click will prevent the app from crashing by trying to remove the same list item twice.
                viewHolder.deleteButton.setEnabled(false);

                // Saving the position of removable item.
                int position = viewHolder.getAdapterPosition();

                // Removing the item from database.
                DatabaseRemover.removeRestaurant(Integer.parseInt(mRestaurantList.get(position).getID()));

                // Remove from university object.
                for (Restaurant r : University.getRestaurantList()) {
                    if (r.getID().equals(mRestaurantList.get(position).getID()))
                    University.removeRestaurantList(r);
                    break;
                }

                // Removing the item from the recyclerview.
                removeItem(position);

                Toast.makeText(mContext, "Restaurant deleted!", Toast.LENGTH_LONG).show();
            }
        });


        viewHolder.availableSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = viewHolder.getAdapterPosition();
                boolean success = DatabaseUpdater.updateRestaurantAvailable(DatabaseInsert.getDatabase(), viewHolder.availableSwitch.isChecked(), mRestaurantList.get(position).getID());
                if (success) {
                    System.out.println("Restaurants status changed.");
                    if (!viewHolder.availableSwitch.isChecked()) {
                        for (Restaurant r : University.getRestaurantList()) {
                            if (r.getID().equals(mRestaurantList.get(position).getID()))
                                r.setAvailableStatus(false);
                            break;
                        }
                        Toast.makeText(mContext, "Restaurant set unavailable.",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mContext, "Restaurant set available.",
                                Toast.LENGTH_LONG).show();
                        for (Restaurant r : University.getRestaurantList()) {
                            if (r.getID().equals(mRestaurantList.get(position).getID()))
                                r.setAvailableStatus(true);
                            break;
                        }
                    }
                } else {
                    System.out.println("Restaurants status wasn't chanced successfully.");
                }

            }
        });
    }

    @Override
    public int getItemCount() {
            return mRestaurantList.size();
            }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name, address, university;
        private LinearLayout reviewListItemLayout;
        private Button deleteButton;
        private Switch availableSwitch;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.restaurantNameInput);
            address = itemView.findViewById(R.id.restaurantAddressInput);
            university = itemView.findViewById(R.id.restaurantUniversityInput);
            deleteButton = itemView.findViewById(R.id.adminRestaurantsDeleteButton);
            availableSwitch = itemView.findViewById(R.id.availableSwitch);
            reviewListItemLayout = itemView.findViewById(R.id.admin_restaurants_list_item);
        }
    }

    private void removeItem(int position) {
        mRestaurantList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mRestaurantList.size());
    }

}

