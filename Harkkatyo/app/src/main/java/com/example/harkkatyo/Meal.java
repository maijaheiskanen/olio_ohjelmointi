package com.example.harkkatyo;

// Contains meals information and reviews given to it.

import java.util.ArrayList;

public class Meal {
    private String name, id;
    private String restaurantID, restaurantName;
    private int type;
    private ArrayList<Review> reviewList = new ArrayList<>();


    public Meal(String n, int t, String pID) {
        name = n;
        type = t;
        id = pID;
    }

    public Meal(String n, int t, String pID, String rID, String rN) {
        name = n;
        type = t;
        id = pID;
        restaurantID = rID;
        restaurantName = rN;
    }

    public String getName() {
        return name;
    }
    public int getType() {
        return type;
    }

    public String getTypeSring() {
        if (type == 1) {
            return "Kotoisia makuja";
        } else if (type == 2) {
            return "Kokin suositus";
        } else if (type == 3) {
            return "Kasvisherkkuja";
        } else {
            return "No type";
        }
    }

    public void updateReviewList() {
        reviewList = DatabaseSearcher.searchReviewsByMealID(DatabaseInsert.getDatabase(), this.getId());
    }

    public void addReviewList(Review r) {
        reviewList.add(r);
    }

    public void removeReviewList(Review r) {
        reviewList.remove(r);
    }

    public ArrayList<Review> getReviewList() {
        return reviewList;
    }

    public String getId() {
        return id;
    }

    public String getRestaurantID() {
        return restaurantID;
    }

    public String getRestaurantName() {
        return restaurantName;
    }
}