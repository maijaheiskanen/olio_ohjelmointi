package com.example.harkkatyo;

// Contains databases tables' and columns' names and stuff.

import android.provider.BaseColumns;

public class TableContract {

    private TableContract() {
    }

    public static final class UniversityEntry implements BaseColumns {
        public static final String TABLE_NAME = "University";
        public static final String COLUMN_UNIVERSITYID = "universityID";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_ADDRESS = "Address";
    }

    /* Didn't want this after all.
    public static final class AdminEntry implements BaseColumns {
        public static final String TABLE_NAME = "Admin";
        public static final String COLUMN_ADMINID = "adminID";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_EMAIL = "Email";
        public static final String COLUMN_PASSWORD = "Password";
    }
    */

    public static final class RestaurantEntry implements BaseColumns {
        public static final String TABLE_NAME = "Restaurant";
        public static final String COLUMN_RESTAURANTID = "restaurantID";
        public static final String COLUMN_UNIVERSITYID = "universityID";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_ADDRESS = "Address";
        public static final String COLUMN_AVAILABLE = "Available";
    }

    public static final class MaintenanceEntry implements BaseColumns {
        public static final String TABLE_NAME = "Maintenance";
        public static final String COLUMN_USERID = "userID";
        public static final String COLUMN_RESTAURANTID = "restaurantID";
    }

    public static final class MealEntry implements BaseColumns {
        public static final String TABLE_NAME = "Meal";
        public static final String COLUMN_MEALID = "mealID";
        public static final String COLUMN_RESTAURANTID = "restaurantID";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_TYPE = "Type";
        public static final String COLUMN_LACTOSEFREE = "LactoseFree";
        public static final String COLUMN_GLUTENFREE = "GlutenFree";
    }

    public static final class MenuEntry implements BaseColumns {
        public static final String TABLE_NAME = "Menu";
        public static final String COLUMN_MENUID = "menuID";
        public static final String COLUMN_RESTAURANTID = "restaurantID";
    }

    /* Didn't want this after all.
    public static final class DateEntry implements BaseColumns {
        public static final String TABLE_NAME = "Date";
        public static final String COLUMN_DATE = "Date";
    }
    */

    public static final class HasDateEntry implements BaseColumns {
        public static final String TABLE_NAME = "HasDate";
        public static final String COLUMN_DATE = "Date";
        public static final String COLUMN_MENUID = "menuID";
    }

    public static final class OnListEntry implements BaseColumns {
        public static final String TABLE_NAME = "OnList";
        public static final String COLUMN_MENUID = "menuID";
        public static final String COLUMN_MEALID = "mealID";
    }

    public static final class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "User";
        public static final String COLUMN_USERID = "userID";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_EMAIL = "Email";
        public static final String COLUMN_PASSWORD = "Password";
        public static final String COLUMN_ISADMIN = "IsAdmin";
    }

    public static final class ReviewEntry implements BaseColumns {
        public static final String TABLE_NAME = "Review";
        public static final String COLUMN_REVIEWID = "reviewID";
        public static final String COLUMN_USERID = "userID";
        public static final String COLUMN_MEALID = "mealID";
        public static final String COLUMN_GARDE = "Grade";
        public static final String COLUMN_VERBALEVALUATION = "VerbalEvaluation";
        public static final String COLUMN_POSOTOVEVOTE = "PositiveVote";
        public static final String COLUMN_NEGATIVEVOTE = "NegativeVote";
        public static final String COLUMN_DATE = "Date";
        public static final String COLUMN_TIME = "Time";
        public static final String COLUMN_SHOWABLE = "Showable";
    }
}
